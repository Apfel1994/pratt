#ifndef SC_SCOPE_H
#define SC_SCOPE_H

#include "sc_syms.h"

typedef struct sc_scope {
    sc_syms defs;
    struct sc_scope *parent;
} sc_scope;

void sc_scope_init(sc_scope *scope);
void sc_scope_destroy(sc_scope *scope);
sc_sym *sc_scope_define(sc_scope *scope, sc_sym sym);
sc_sym *sc_scope_find(sc_scope *scope, const char *id, const char *val);
sc_sym *sc_scope_reserve(sc_scope *scope, sc_sym sym);

#endif /* SC_SCOPE_H */
