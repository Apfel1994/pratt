#include "test_assert.h"

#include <stdio.h>
#include <string.h>

#include "sc_par.h"
#include "sc_scope.h"

#pragma warning(disable : 4996)

typedef struct sc_syms_blck {
    sc_sym *syms;
    int len, cap;
    struct sc_syms_blck *next;
} sc_syms_blck;

static sc_sym *_cmp_led(sc_par_state *state, sc_sym *self, sc_sym *left) {
	self->first = left;
	self->second = sc_par_expression(state, 40);
	self->arity = AR_BINARY;
	return self;
}

static sc_sym *_plus_minus_led(sc_par_state *state, sc_sym *self, sc_sym *left) {
	self->first = left;
	self->second = sc_par_expression(state, 50);
	self->arity = AR_BINARY;
	return self;
}

static sc_sym *_mul_div_led(sc_par_state *state, sc_sym *self, sc_sym *left) {
	self->first = left;
	self->second = sc_par_expression(state, 60);
	self->arity = AR_BINARY;
	return self;
}

static sc_sym *_ternary_led(sc_par_state *state, sc_sym *self, sc_sym *left) {
	self->first = left;
	self->second = sc_par_expression(state, 0);
	sc_par_advance(state, ":");
	self->third = sc_par_expression(state, 0);
	self->arity = AR_TERNARY;
	return self;
}

static sc_sym *_dot_led(sc_par_state *state, sc_sym *self, sc_sym *left) {
	self->first = left;
	if (state->token->arity != AR_NAME) {
		state->token->error = "Expected a property name.";
	}
	state->token->arity = AR_LITERAL;
	self->second = state->token;
	self->arity = AR_BINARY;
	sc_par_advance(state, NULL);
	return self;
}

static sc_sym *_assign_led(sc_par_state *state, sc_sym *self, sc_sym *left) {
	if (strcmp(left->id, ".") != 0 && strcmp(left->id, "[") != 0 && left->arity != AR_NAME) {
		left->error = "Bad lvalue.";
	}
	self->first = left;
	self->second = sc_par_expression(state, 9);
	self->assignment = 1;
	self->arity = AR_BINARY;
	return self;
}

static sc_sym *_literal_nud(sc_par_state *state, sc_sym *self) {
	return self;
}

static sc_sym *_prefix_nud(sc_par_state *state, sc_sym *self) {
	self->first = sc_par_expression(state, 70);
	self->arity = AR_UNARY;
	return self;
}

static sc_sym *_bracket_nud(sc_par_state *state, sc_sym *self) {
	sc_sym *e = sc_par_expression(state, 0);
	sc_par_advance(state, ")");
	return e;
}

static sc_sym *_constant_nud(sc_par_state *state, sc_sym *self) {
	sc_scope_reserve(state->scope, *self);
	self->value = sc_syms_find(&state->syms, self->id, NULL)->value;
	self->arity = AR_LITERAL;
	return self;
}

static void _var_std(sc_par_state *state, struct sc_sym *self, sc_syms *ret) {
	while (1) {
		sc_sym *n = state->token;
		if (n->arity != AR_NAME) {
			n->error = "Expected a new variable name.";
		}
		sc_scope_define(state->scope, *n);
		sc_par_advance(state, NULL);

		if (strcmp(state->token->id, "=") == 0) {
			sc_sym *t = state->token;
			sc_par_advance(state, "=");
			t->first = n;
			t->second = sc_par_expression(state, 0);
			t->arity = AR_BINARY;

			sc_syms_push(ret, *t);
		}
		if (strcmp(state->token->id, ",") != 0) {
			break;
		}
		sc_par_advance(state, ",");
	}
	sc_par_advance(state, ";");
}

static void _block_std(sc_par_state *state, struct sc_sym *self, sc_syms *ret) {
    sc_par_push_scope(state);
    sc_syms st = sc_par_statements(state);
    sc_par_advance(state, "}");
    sc_par_pop_scope(state);
    sc_syms_insert(ret, st);
}

static sc_sym *define_statement(sc_par_state *state, const char *id, sc_std_func f) {
	sc_sym *s = sc_par_create_symbol(state, id, 0);
	s->std = f;
	return s;
}

static void test_init_state(sc_par_state *state) {
	sc_par_state_init(state);

	sc_par_create_symbol(state, "(literal)", 0)->nud = _literal_nud;
	sc_par_create_symbol(state, "==", 40)->led = _cmp_led;
	sc_par_create_symbol(state, "!=", 40)->led = _cmp_led;
	sc_par_create_symbol(state, "<", 40)->led = _cmp_led;
	sc_par_create_symbol(state, "<=", 40)->led = _cmp_led;
	sc_par_create_symbol(state, ">", 40)->led = _cmp_led;
	sc_par_create_symbol(state, ">=", 40)->led = _cmp_led;
	sc_par_create_symbol(state, "+", 50)->led = _plus_minus_led;

	sc_par_create_symbol(state, "=", 10)->led = _assign_led;
	sc_par_create_symbol(state, "+=", 10)->led = _assign_led;
	sc_par_create_symbol(state, "-=", 10)->led = _assign_led;

	sc_sym *s = sc_par_create_symbol(state, "-", 50);
	s->nud = _prefix_nud;
	s->led = _plus_minus_led;

	sc_par_create_symbol(state, "*", 60)->led = _mul_div_led;
	sc_par_create_symbol(state, "/", 60)->led = _mul_div_led;

	sc_par_create_symbol(state, "?", 20)->led = _ternary_led;
	sc_par_create_symbol(state, ":", 0);

	sc_par_create_symbol(state, "(", 0)->nud = _bracket_nud;
	sc_par_create_symbol(state, ")", 0);

	sc_par_create_symbol(state, "!", 0)->nud = _prefix_nud;

	sc_par_create_symbol(state, ".", 80)->led = _dot_led;

	sc_par_create_symbol(state, "(end)", 0);
	sc_par_create_symbol(state, "(name)", 0);
	sc_par_create_symbol(state, ";", 0);
	sc_par_create_symbol(state, ",", 0);

	sc_par_create_constant(state, "true", "true", _constant_nud);
	sc_par_create_constant(state, "false", "false", _constant_nud);
	sc_par_create_constant(state, "null", "null", _constant_nud);
	sc_par_create_constant(state, "pi", "3.141592653589793", _constant_nud);

	define_statement(state, "var", _var_std);
    define_statement(state, "{", _block_std);
    sc_par_create_symbol(state, "}", 0);
}

void test_par() {
	{	/* test syms */
		sc_syms syms;
		sc_syms_init(&syms);

		require(syms.head == syms.tail);

		sc_sym s; s.id = NULL;

		int i;
		for (i = 0; i < 2048; i++) {
			sc_syms_push(&syms, s);
		}

		require(sc_syms_len(&syms) > 1);

		sc_syms_destroy(&syms);
	}

#define PUSH_TOK(t, v)			\
	{							\
		sc_tok tok;				\
		tok.type = (t);			\
		tok.val = strdup(v);	\
		sc_toks_push(&state.toks, tok); \
	}

	{	/* test simple expression */
		sc_par_state state;
		test_init_state(&state);

		PUSH_TOK(TOK_OPERATOR, "-")
		PUSH_TOK(TOK_NUMBER,   "1")
		PUSH_TOK(TOK_OPERATOR, "+")
		PUSH_TOK(TOK_NUMBER,   "2")
		PUSH_TOK(TOK_OPERATOR, "*")
		PUSH_TOK(TOK_NUMBER,   "3")
		PUSH_TOK(TOK_OPERATOR, "<")
		PUSH_TOK(TOK_NUMBER,   "12")

		sc_par_advance(&state, NULL);
		sc_sym *s = sc_par_expression(&state, 0);

		require(strcmp(s->id, "<") == 0);
		require(s->arity == AR_BINARY);
		require(strcmp(s->second->id, "(literal)") == 0);
		require(strcmp(s->second->value, "12") == 0);

		s = s->first;
		require(strcmp(s->id, "+") == 0);

		require(strcmp(s->first->id, "-") == 0);
		require(s->first->arity == AR_UNARY);
		require(strcmp(s->first->first->id, "(literal)") == 0);
		require(strcmp(s->first->first->value, "1") == 0);

		require(strcmp(s->second->id, "*") == 0);
		require(s->second->arity == AR_BINARY);
		require(strcmp(s->second->first->id, "(literal)") == 0);
		require(strcmp(s->second->first->value, "2") == 0);
		require(strcmp(s->second->second->id, "(literal)") == 0);
		require(strcmp(s->second->second->value, "3") == 0);

		sc_par_state_destroy(&state);
	}

	{	/* test ternary operator */
		sc_par_state state;
		test_init_state(&state);

		PUSH_TOK(TOK_NUMBER, "1")
		PUSH_TOK(TOK_OPERATOR, ">")
		PUSH_TOK(TOK_NUMBER, "2")
		PUSH_TOK(TOK_OPERATOR, "?")
		PUSH_TOK(TOK_NUMBER, "3")
		PUSH_TOK(TOK_OPERATOR, ":")
		PUSH_TOK(TOK_NUMBER, "2")
		PUSH_TOK(TOK_OPERATOR, "+")
		PUSH_TOK(TOK_NUMBER, "1")

		sc_par_advance(&state, NULL);
		sc_sym *s = sc_par_expression(&state, 0);

		require(strcmp(s->id, "?") == 0);

		require(strcmp(s->first->id, ">") == 0);
		require(strcmp(s->first->first->id, "(literal)") == 0);
		require(strcmp(s->first->first->value, "1") == 0);
		require(strcmp(s->first->second->id, "(literal)") == 0);
		require(strcmp(s->first->second->value, "2") == 0);

		require(strcmp(s->second->id, "(literal)") == 0);
		require(strcmp(s->second->value, "3") == 0);

		require(strcmp(s->third->id, "+") == 0);
		require(strcmp(s->third->first->id, "(literal)") == 0);
		require(strcmp(s->third->first->value, "2") == 0);
		require(strcmp(s->third->second->id, "(literal)") == 0);
		require(strcmp(s->third->second->value, "1") == 0);

		sc_par_state_destroy(&state);
	}

	{	/* test brackets */
		sc_par_state state;
		test_init_state(&state);

		PUSH_TOK(TOK_OPERATOR, "(")
		PUSH_TOK(TOK_NUMBER, "1")
		PUSH_TOK(TOK_OPERATOR, "+")
		PUSH_TOK(TOK_NUMBER, "2")
		PUSH_TOK(TOK_OPERATOR, ")")
		PUSH_TOK(TOK_OPERATOR, "*")
		PUSH_TOK(TOK_NUMBER, "3")

		sc_par_advance(&state, NULL);
		sc_sym *s = sc_par_expression(&state, 0);

		require(strcmp(s->id, "*") == 0);
		require(strcmp(s->first->id, "+") == 0);
		require(strcmp(s->first->first->id, "(literal)") == 0);
		require(strcmp(s->first->first->value, "1") == 0);
		require(strcmp(s->first->second->id, "(literal)") == 0);
		require(strcmp(s->first->second->value, "2") == 0);
		require(strcmp(s->second->id, "(literal)") == 0);
		require(strcmp(s->second->value, "3") == 0);

		sc_par_state_destroy(&state);
	}

	{   /* constants */
		sc_par_state state;
		test_init_state(&state);

		PUSH_TOK(TOK_NAME, "true")
		PUSH_TOK(TOK_OPERATOR, "-")
		PUSH_TOK(TOK_NAME, "false")
		PUSH_TOK(TOK_OPERATOR, "+")
		PUSH_TOK(TOK_NAME, "pi")

		sc_par_push_scope(&state);
		sc_par_advance(&state, NULL);
		sc_sym *s = sc_par_expression(&state, 0);

		require(strcmp(s->id, "+") == 0);
		require(strcmp(s->first->id, "-") == 0);
		require(strcmp(s->first->first->id, "true") == 0);
		require(strcmp(s->first->first->value, "true") == 0);
		require(strcmp(s->first->second->id, "false") == 0);
		require(strcmp(s->first->second->value, "false") == 0);
		require(strcmp(s->second->id, "pi") == 0);
		require(strcmp(s->second->value, "3.141592653589793") == 0);

		sc_par_pop_scope(&state);
		sc_par_state_destroy(&state);
	}

	{	/* var statement */
		sc_par_state state;
		test_init_state(&state);

		PUSH_TOK(TOK_NAME, "var")
		PUSH_TOK(TOK_NAME, "x")
		PUSH_TOK(TOK_OPERATOR, "=")
		PUSH_TOK(TOK_NUMBER, "1")
		PUSH_TOK(TOK_OPERATOR, "+")
		PUSH_TOK(TOK_NUMBER, "2")
		PUSH_TOK(TOK_OPERATOR, ",")
		PUSH_TOK(TOK_NAME, "y")
		PUSH_TOK(TOK_OPERATOR, "=")
		PUSH_TOK(TOK_NUMBER, "3")
		PUSH_TOK(TOK_OPERATOR, ";")
		PUSH_TOK(TOK_NAME, "(end)")

		sc_par_push_scope(&state);
		sc_par_advance(&state, NULL);
		sc_syms st = sc_par_statements(&state);

		const sc_sym *s;

		s = &st.head->syms[0];
		require(strcmp(s->id, "=") == 0);
		require(strcmp(s->first->id, "(name)") == 0);
		require(strcmp(s->first->value, "x") == 0);
		require(strcmp(s->second->id, "+") == 0);
		require(strcmp(s->second->first->id, "(literal)") == 0);
		require(strcmp(s->second->first->value, "1") == 0);
		require(strcmp(s->second->second->id, "(literal)") == 0);
		require(strcmp(s->second->second->value, "2") == 0);

		s = &st.head->syms[1];
		require(strcmp(s->id, "=") == 0);
		require(strcmp(s->first->id, "(name)") == 0);
		require(strcmp(s->first->value, "y") == 0);
		require(strcmp(s->second->id, "(literal)") == 0);
		require(strcmp(s->second->value, "3") == 0);

		sc_syms_destroy(&st);
		sc_par_pop_scope(&state);
		sc_par_state_destroy(&state);
	}

    {	/* var statement */
        sc_par_state state;
        test_init_state(&state);

        PUSH_TOK(TOK_OPERATOR, "{")
        PUSH_TOK(TOK_NAME, "var")
        PUSH_TOK(TOK_NAME, "x")
        PUSH_TOK(TOK_OPERATOR, "=")
        PUSH_TOK(TOK_NUMBER, "1")
        PUSH_TOK(TOK_OPERATOR, "+")
        PUSH_TOK(TOK_NUMBER, "2")
        PUSH_TOK(TOK_OPERATOR, ";")
        PUSH_TOK(TOK_OPERATOR, "}")
        PUSH_TOK(TOK_NAME, "(end)")

        sc_par_push_scope(&state);
        sc_par_advance(&state, NULL);
        sc_syms st = sc_par_statements(&state);

        const sc_sym *s;

        s = &st.head->syms[0];
        require(strcmp(s->id, "=") == 0);
        require(strcmp(s->first->id, "(name)") == 0);
        require(strcmp(s->first->value, "x") == 0);
        require(strcmp(s->second->id, "+") == 0);
        require(strcmp(s->second->first->id, "(literal)") == 0);
        require(strcmp(s->second->first->value, "1") == 0);
        require(strcmp(s->second->second->id, "(literal)") == 0);
        require(strcmp(s->second->second->value, "2") == 0);

        sc_syms_destroy(&st);
        sc_par_pop_scope(&state);
        sc_par_state_destroy(&state);
    }

	{	/* test dot operator */
		sc_par_state state;
		test_init_state(&state);

		PUSH_TOK(TOK_NAME, "var")
		PUSH_TOK(TOK_NAME, "obj")
		PUSH_TOK(TOK_OPERATOR, ";")
		PUSH_TOK(TOK_NAME, "obj")
		PUSH_TOK(TOK_OPERATOR, ".")
		PUSH_TOK(TOK_NAME, "b")
		PUSH_TOK(TOK_OPERATOR, "=")
		PUSH_TOK(TOK_NUMBER, "16")
		PUSH_TOK(TOK_OPERATOR, "-")
        PUSH_TOK(TOK_OPERATOR, "(")
		PUSH_TOK(TOK_NUMBER, "12")
        PUSH_TOK(TOK_OPERATOR, "-")
        PUSH_TOK(TOK_NUMBER, "8")
        PUSH_TOK(TOK_OPERATOR, ")")

		sc_par_push_scope(&state);
		sc_par_advance(&state, NULL);
		sc_syms st = sc_par_statements(&state);

		const sc_sym *s = &st.head->syms[0];
		require(strcmp(s->id, "=") == 0);
		require(strcmp(s->first->id, ".") == 0);
		require(strcmp(s->second->id, "-") == 0);
		require(strcmp(s->second->first->id, "(literal)") == 0);
		require(strcmp(s->second->first->value, "16") == 0);
		require(strcmp(s->second->second->id, "-") == 0);
        require(strcmp(s->second->second->first->id, "(literal)") == 0);
        require(strcmp(s->second->second->first->value, "12") == 0);
        require(strcmp(s->second->second->second->id, "(literal)") == 0);
        require(strcmp(s->second->second->second->value, "8") == 0);

		sc_syms_destroy(&st);
		sc_par_pop_scope(&state);
		sc_par_state_destroy(&state);
	}
}