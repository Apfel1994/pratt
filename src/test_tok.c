#include "test_assert.h"

#include <string.h>

#include "sc_tok.h"

void test_tok() {
    {   /* empty string */
        sc_toks t = { 0 };
        require(sc_tokenize("", &t) == 0);
        require(t.len == 0);
        sc_toks_destroy(&t);
    }
    
    {   /* name */
        sc_toks t = { 0 };
        require(sc_tokenize("_name", &t) == 0);
        require(t.len == 1);
        require(t.toks[0].type == TOK_NAME);
        require(strcmp(t.toks[0].val, "_name") == 0);
        sc_toks_destroy(&t);
    }

    {   /* number */
        sc_toks t = { 0 };
        require(sc_tokenize("12 3.145 2.3e4", &t) == 0);
        require(t.len == 3);
        require(t.toks[0].type == TOK_NUMBER);
        require(strcmp(t.toks[0].val, "12") == 0);
        require(t.toks[1].type == TOK_NUMBER);
        require(strcmp(t.toks[1].val, "3.145") == 0);
        require(t.toks[2].type == TOK_NUMBER);
        require(strcmp(t.toks[2].val, "2.3e4") == 0);
        sc_toks_destroy(&t);

        require(sc_tokenize("14ey", &t) == -1);
        require(t.len == 1);
        require(t.toks[0].type == TOK_NUMBER);
        require(strcmp(t.toks[0].error, "Bad exponent") == 0);
        sc_toks_destroy(&t);

        require(sc_tokenize("4.56.3", &t) == -1);
        require(t.len == 1);
        require(t.toks[0].type == TOK_NUMBER);
        require(strcmp(t.toks[0].error, "Bad number") == 0);
        sc_toks_destroy(&t);
    }

    {   /* string */
        sc_toks t = { 0 };
        require(sc_tokenize("\"my string!\" \"QWERTadsf123@@@$$$$\"", &t) == 0);
        require(t.len == 2);
        require(t.toks[0].type == TOK_STRING);
        require(strcmp(t.toks[0].val, "my string!") == 0);
        require(t.toks[1].type == TOK_STRING);
        require(strcmp(t.toks[1].val, "QWERTadsf123@@@$$$$") == 0);
        sc_toks_destroy(&t);

        require(sc_tokenize("\"my string!", &t) == -1);
        require(t.len == 1);
        require(t.toks[0].type == TOK_STRING);
        require(strcmp(t.toks[0].error, "Unterminated string.") == 0);
        sc_toks_destroy(&t);

        require(sc_tokenize("\"my string!'", &t) == -1);
        require(t.len == 1);
        require(t.toks[0].type == TOK_STRING);
        require(strcmp(t.toks[0].error, "Unterminated string.") == 0);
        sc_toks_destroy(&t);
    }

    {   /* comment */
        sc_toks t = { 0 };
        require(sc_tokenize("// comment 1 \n // comment 2", &t) == 0);
        sc_toks_destroy(&t);
    }

    {   /* operators */
        sc_toks t = { 0 };
		require(sc_tokenize("b =1+ a - (c * 8)", &t) == 0);
		require(t.len == 11);
		require(t.toks[0].type == TOK_NAME);
		require(strcmp(t.toks[0].val, "b") == 0);
		require(t.toks[1].type == TOK_OPERATOR);
		require(strcmp(t.toks[1].val, "=") == 0);
		require(t.toks[2].type == TOK_NUMBER);
		require(strcmp(t.toks[2].val, "1") == 0);
		require(t.toks[3].type == TOK_OPERATOR);
		require(strcmp(t.toks[3].val, "+") == 0);
		require(t.toks[4].type == TOK_NAME);
		require(strcmp(t.toks[4].val, "a") == 0);
		require(t.toks[5].type == TOK_OPERATOR);
		require(strcmp(t.toks[5].val, "-") == 0);
		require(t.toks[6].type == TOK_OPERATOR);
		require(strcmp(t.toks[6].val, "(") == 0);
		require(t.toks[7].type == TOK_NAME);
		require(strcmp(t.toks[7].val, "c") == 0);
		require(t.toks[8].type == TOK_OPERATOR);
		require(strcmp(t.toks[8].val, "*") == 0);
		require(t.toks[9].type == TOK_NUMBER);
		require(strcmp(t.toks[9].val, "8") == 0);
		require(t.toks[10].type == TOK_OPERATOR);
		require(strcmp(t.toks[10].val, ")") == 0);
		sc_toks_destroy(&t);

		require(sc_tokenize("obj.b = 16 + 12", &t) == 0);
		require(t.len == 7);
		require(t.toks[0].type == TOK_NAME);
		require(strcmp(t.toks[0].val, "obj") == 0);
		require(t.toks[1].type == TOK_OPERATOR);
		require(strcmp(t.toks[1].val, ".") == 0);
		require(t.toks[2].type == TOK_NAME);
		require(strcmp(t.toks[2].val, "b") == 0);
		require(t.toks[3].type == TOK_OPERATOR);
		require(strcmp(t.toks[3].val, "=") == 0);
		require(t.toks[4].type == TOK_NUMBER);
		require(strcmp(t.toks[4].val, "16") == 0);
		require(t.toks[5].type == TOK_OPERATOR);
		require(strcmp(t.toks[5].val, "+") == 0);
		require(t.toks[6].type == TOK_NUMBER);
		require(strcmp(t.toks[6].val, "12") == 0);
		sc_toks_destroy(&t);
    }
}
