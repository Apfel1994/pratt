#include "test_assert.h"

#include <stdio.h>
#include <string.h>

#include "sc_str_pool.h"

void test_str_pool() {
    sc_str_pool p;
    int i;
    const char *s1[1024], *s2[1024];
    sc_str_pool_init(&p);

    for (i = 0; i < 1024; i++) {
        s1[i] = sc_str_pool_alloc(&p, "string123456", 0);

        char str[16];
        sprintf(str,"%d",i);
        s2[i] = sc_str_pool_alloc(&p, str, 0);
    }

    for (i = 0; i < 1024; i++) {
        char str[16];
        sprintf(str,"%d",i);

        require(strcmp(s1[i], "string123456") == 0);
        require(strcmp(s2[i], str) == 0);
    }

    sc_str_pool_destroy(&p);
}