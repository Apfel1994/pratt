#ifndef SC_TOK_H
#define SC_TOK_H

#include "sc_str_pool.h"

typedef enum eScTokType {
	TOK_NAME,
	TOK_STRING,
	TOK_NUMBER,
	TOK_OPERATOR,
} eScTokType;

typedef struct sc_tok {
	eScTokType type;
	const char *val;
	char *error;
} sc_tok;

typedef struct sc_toks {
	sc_tok *toks;
	int len, cap;
    sc_str_pool str_pool;
} sc_toks;

void sc_toks_init(sc_toks *t);
void sc_toks_destroy(sc_toks *t);
void sc_toks_push(sc_toks *toks, sc_tok tok);

void sc_toks_print(sc_toks *t);

int sc_tokenize(const char *src, sc_toks *toks);

#endif /* SC_TOK_H */