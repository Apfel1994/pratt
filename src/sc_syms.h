#ifndef SC_SYMS_H
#define SC_SYMS_H

typedef enum e_sc_sym_arity {
    AR_NONE,
    AR_NAME,
    AR_LITERAL,
    AR_OPERATOR,
    AR_UNARY,
    AR_BINARY,
    AR_TERNARY,
} e_sc_sym_arity;

struct sc_par_state;
struct sc_sym;
struct sc_syms;

typedef struct sc_sym *(*sc_nud_func)(struct sc_par_state *state, struct sc_sym *self);
typedef struct sc_sym *(*sc_led_func)(struct sc_par_state *state, struct sc_sym *self, struct sc_sym *left);
typedef void (*sc_std_func)(struct sc_par_state *state, struct sc_sym *self, struct sc_syms *ret);

typedef struct sc_sym {
    const char *id, *value;
    int lbp;
    e_sc_sym_arity arity;
    struct sc_sym *first, *second, *third;
    struct sc_scope *scope;
    int reserved, assignment;
    const char *error;

	sc_nud_func nud;
	sc_led_func led;
	sc_std_func std;
} sc_sym;

typedef struct sc_syms {
    struct sc_syms_blck *head, *tail;
} sc_syms;

void sc_syms_init(sc_syms *s);
void sc_syms_destroy(sc_syms *s);
sc_sym *sc_syms_push(sc_syms *s, sc_sym sym);
sc_sym *sc_syms_find(sc_syms *s, const char *id, const char *val);
int sc_syms_len(sc_syms *s);
void sc_syms_insert(sc_syms *s, const sc_syms syms);

void sc_sym_print(sc_sym *s);
void sc_syms_print(sc_syms *s);

#endif /* SC_SYMS_H */
