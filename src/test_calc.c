#include "test_assert.h"

#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "sc_par.h"
#include "sc_scope.h"

static sc_sym *_plus_minus_led(sc_par_state *state, sc_sym *self, sc_sym *left) {
    self->first = left;
    self->second = sc_par_expression(state, 50);
    self->arity = AR_BINARY;
    return self;
}

static sc_sym *_mul_div_led(sc_par_state *state, sc_sym *self, sc_sym *left) {
    self->first = left;
    self->second = sc_par_expression(state, 60);
    self->arity = AR_BINARY;
    return self;
}

static sc_sym *_literal_nud(sc_par_state *state, sc_sym *self) {
    return self;
}

static sc_sym *_prefix_nud(sc_par_state *state, sc_sym *self) {
    self->first = sc_par_expression(state, 70);
    self->arity = AR_UNARY;
    return self;
}

static sc_sym *_bracket_nud(sc_par_state *state, sc_sym *self) {
    sc_sym *e = sc_par_expression(state, 0);
    sc_par_advance(state, ")");
    return e;
}

static void _var_std(sc_par_state *state, struct sc_sym *self, sc_syms *ret) {
    while (1) {
        sc_sym *n = state->token;
        if (n->arity != AR_NAME) {
            n->error = "Expected a new variable name.";
        }
        sc_scope_define(state->scope, *n);
        sc_par_advance(state, NULL);

        if (strcmp(state->token->id, "=") == 0) {
            sc_sym *t = state->token;
            sc_par_advance(state, "=");
            t->first = n;
            t->second = sc_par_expression(state, 0);
            t->arity = AR_BINARY;

            sc_syms_push(ret, *t);
        }
        if (strcmp(state->token->id, ",") != 0) {
            break;
        }
        sc_par_advance(state, ",");
    }
    sc_par_advance(state, ";");
}

static void test_init_state(sc_par_state *state) {
    sc_par_state_init(state);

    sc_par_create_symbol(state, "(literal)", 0)->nud = _literal_nud;
    sc_par_create_symbol(state, "+", 50)->led = _plus_minus_led;

    sc_sym *s = sc_par_create_symbol(state, "-", 50);
    s->nud = _prefix_nud;
    s->led = _plus_minus_led;

    sc_par_create_symbol(state, "*", 60)->led = _mul_div_led;
    sc_par_create_symbol(state, "/", 60)->led = _mul_div_led;

    sc_par_create_symbol(state, "(", 0)->nud = _bracket_nud;
    sc_par_create_symbol(state, ")", 0);

    sc_par_create_symbol(state, "(end)", 0);
}

typedef double real;

static real calc(sc_sym *s) {
    if (s->arity == AR_LITERAL) {
        return atof(s->value);
    } else if (s->arity == AR_BINARY) {
        if (strcmp(s->id, "+") == 0) {
            return calc(s->first) + calc(s->second);
        } else if (strcmp(s->id, "-") == 0) {
            return calc(s->first) - calc(s->second);
        } else if (strcmp(s->id, "*") == 0) {
            return calc(s->first) * calc(s->second);
        } else if (strcmp(s->id, "/") == 0) {
            return calc(s->first) / calc(s->second);
        }
    }
    return 0;
}

static void require_eq_approx(real v1, real v2, real d) {
    require(fabs(v1 - v2) < d);
}

void test_calc() {
    {   /* Simple expression */
        sc_par_state state;
        test_init_state(&state);

        require(sc_par_tokenize(&state, "4 + 5.25 + 8 * (12 - 3)") == 0);

        sc_par_advance(&state, NULL);
        sc_sym *s = sc_par_expression(&state, 0);

        real res = calc(s);
        require_eq_approx(res, (real)81.25, (real)0.0001);

        sc_par_state_destroy(&state);
    }
}