#include "sc_par.h"

#include <assert.h>
#include <stdlib.h>
#include <string.h>

#include "sc_scope.h"

///////////

static sc_sym *_sc_nud_default(sc_par_state *state, sc_sym *self) {
	self->error = "Undefined.";
	return NULL;
}

static sc_sym *_sc_led_default(sc_par_state *state, sc_sym *self, struct sc_sym *left) {
	self->error = "Missing operator.";
	return NULL;
}

//////////////////////////////////////////////////////////////////////


void sc_par_state_init(sc_par_state *state) {
	sc_toks_init(&state->toks);
	sc_syms_init(&state->syms);

    state->scope = NULL;
	state->token = NULL;
	state->token_nr = 0;
}

void sc_par_state_destroy(sc_par_state *state) {
	sc_toks_destroy(&state->toks);
	sc_syms_destroy(&state->syms);

    assert(state->scope == NULL);
}

sc_sym *sc_par_create_symbol(sc_par_state *state, const char *id, int bp) {
	sc_sym *s = sc_syms_find(&state->syms, id, NULL);
	if (s) {
		if (bp >= s->lbp) {
			s->lbp = bp;
		}
	} else {
		sc_sym new_sym;
		s = &new_sym;
		s->id = s->value = id;
		s->lbp = bp;
		s->nud = _sc_nud_default;
		s->led = _sc_led_default;
        s->std = NULL;
        s->reserved = 0;
		s->assignment = 0;
        s->first = s->second = s->third = NULL;
		s = sc_syms_push(&state->syms, *s);
	}
	return s;
}

sc_sym *sc_par_create_constant(sc_par_state *state, const char *id, const char *val, sc_nud_func nud) {
	sc_sym *s = sc_par_create_symbol(state, id, 0);
	s->nud = nud;
	s->value = val;
	return s;
}

int sc_par_tokenize(sc_par_state *state, const char *str) {
    sc_toks_destroy(&state->toks);
    return sc_tokenize(str, &state->toks);
}

sc_scope *sc_par_push_scope(sc_par_state *state) {
    sc_scope *s = malloc(sizeof(sc_scope));
    sc_scope_init(s);

    s->parent = state->scope;
    state->scope = s;
    return state->scope;
}

void sc_par_pop_scope(sc_par_state *state) {
    assert(state->scope);
    sc_scope *s = state->scope;
    state->scope = s->parent;

    sc_scope_destroy(s);
    free(s);
}

sc_sym *sc_par_find_in_scope(sc_par_state *state, const char *v) {
    sc_sym *s = sc_scope_find(state->scope, NULL, v);
	if (!s) s = sc_syms_find(&state->syms, v, NULL);
    if (!s) s = sc_syms_find(&state->syms, "(name)", NULL);
    return s;
}

sc_sym *sc_par_advance(sc_par_state *state, const char *id) {
	sc_sym o = { NULL };

	if (id && strcmp(state->token->id, id) != 0) {
		// TODO: dynamic string for error
		state->token->error = "Expected.";
	}
	if (state->token_nr >= state->toks.len) {
		state->token = sc_syms_find(&state->syms, "(end)", NULL);
		return NULL;
	}

	sc_tok *t = &state->toks.toks[state->token_nr++];
	const char *v = t->val;
    e_sc_sym_arity a = AR_NONE;

	if (t->type == TOK_NAME) {
		a = AR_NAME;
        o = *sc_par_find_in_scope(state, v);
	} else if (t->type == TOK_OPERATOR) {
		a = AR_OPERATOR;
		sc_sym *op = sc_syms_find(&state->syms, NULL, v);
		if (op) {
			o = *op;
		} else {
			t->error = "Unknown operator.";
		}
	} else if (t->type == TOK_STRING || t->type == TOK_NUMBER) {
		a = AR_LITERAL;
		o = *sc_syms_find(&state->syms, "(literal)", NULL);
	} else {
		t->error = "Unexpected token.";
	}

	state->token = sc_syms_push(&state->syms, o);
	state->token->value = v;
	state->token->arity = a;

	return state->token;
}

sc_sym *sc_par_expression(sc_par_state *state, int rbp) {
	sc_sym *t = state->token;
	sc_par_advance(state, NULL);
	sc_sym *left = t->nud(state, t);
	while (state->token && rbp < state->token->lbp) {
		t = state->token;
		sc_par_advance(state, NULL);
		left = t->led(state, t, left);
	}
	return left;
}

void sc_par_statement(sc_par_state *state, sc_syms *ret) {
	sc_sym *t = state->token;
	if (t->std) {
		sc_par_advance(state, NULL);
		sc_scope_reserve(state->scope, *t);
		t->std(state, t, ret);
		return;
	}
	sc_sym *v = sc_par_expression(state, 0);
	if (!v->assignment && strcmp(v->id, "(") != 0) {
		v->error = "Bad expression statement.";
	}
	sc_syms_push(ret, *v);
	sc_par_advance(state, ";");
}

sc_syms sc_par_statements(sc_par_state *state) {
	sc_syms ret;
	sc_syms_init(&ret);
	while (1) {
		sc_sym *t = state->token;
		if (strcmp(t->id, "}") == 0 || strcmp(t->id, "(end)") == 0) {
			break;
		}
		sc_par_statement(state, &ret);
	}
	return ret;
}
