#include "sc_syms.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void sc_empty_free(void *_) {}

typedef struct sc_syms_blck {
    sc_sym *syms;
    int len, cap;
    struct sc_syms_blck *next;
} sc_syms_blck;

void sc_syms_blck_init(sc_syms_blck *s, int cap, sc_sym *syms) {
    if (syms) {
        s->syms = syms;
    } else {
        s->syms = malloc(cap * sizeof(sc_sym));
    }
    s->cap = cap;
    s->len = 0;
    s->next = NULL;
}

void sc_syms_blck_destroy(sc_syms_blck *s, void (*syms_free)(void *)) {
    if (syms_free) {
        syms_free(s->syms);
    } else {
        free(s->syms);
    }
    s->syms = NULL;
    s->len = s->cap = 0;
}

sc_sym *sc_syms_blck_push(sc_syms_blck *s, sc_sym sym) {
    if (s->len == s->cap) {
        return NULL;
    }
    s->syms[s->len] = sym;
    return &s->syms[s->len++];
}

sc_sym *sc_syms_blck_find(sc_syms_blck *s, const char *id, const char *val) {
    int i;
	if (id) {
		for (i = 0; i < s->len; i++) {
			if (strcmp(s->syms[i].id, id) == 0) {
				return &s->syms[i];
			}
		}
	} else if (val) {
		for (i = 0; i < s->len; i++) {
			if (strcmp(s->syms[i].value, val) == 0) {
				return &s->syms[i];
			}
		}
	}
    return NULL;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void sc_syms_init(sc_syms *s) {
    s->head = s->tail = malloc(sizeof(sc_syms_blck) + 64 * sizeof(sc_sym));
	sc_sym *syms = (sc_sym *)((uintptr_t)s->head + sizeof(sc_syms_blck));
    sc_syms_blck_init(s->head, 64, syms);
}

void sc_syms_destroy(sc_syms *s) {
    sc_syms_blck *cur_blck = s->head;
    while (cur_blck) {
        sc_syms_blck *next = cur_blck->next;
        sc_syms_blck_destroy(cur_blck, sc_empty_free);
        free(cur_blck);
        cur_blck = next;
    }
    s->head = s->tail = NULL;
}

sc_sym *sc_syms_push(sc_syms *s, sc_sym sym) {
    sc_sym *ret = sc_syms_blck_push(s->tail, sym);
    if (!ret) {
        s->tail->next = malloc(sizeof(sc_syms_blck) + s->tail->cap * 2 * sizeof(sc_sym));
		sc_sym *syms = (sc_sym *)((uintptr_t)s->tail->next + sizeof(sc_syms_blck));
        sc_syms_blck_init(s->tail->next, s->tail->cap * 2, syms);
        s->tail = s->tail->next;
        ret = sc_syms_blck_push(s->tail, sym);
    }
    return ret;
}

sc_sym *sc_syms_find(sc_syms *s, const char *id, const char *val) {
    sc_syms_blck *cur_blck = s->head;
    while (cur_blck) {
        sc_sym *ret = sc_syms_blck_find(cur_blck, id, val);
        if (ret) return ret;
        cur_blck = cur_blck->next;
    }
    return NULL;
}

int sc_syms_len(sc_syms *s) {
    int ret = 0;
    sc_syms_blck *cur_blck = s->head;
    while (cur_blck) {
        ret++;
        cur_blck = cur_blck->next;
    }
    return ret;
}

void sc_syms_insert(sc_syms *s, const sc_syms syms) {
	sc_syms_blck *cur_blck = syms.head;
	while (cur_blck) {
		int i;
		for (i = 0; i < cur_blck->len; i++) {
			sc_syms_push(s, cur_blck->syms[i]);
		}
		cur_blck = cur_blck->next;
	}
}

static void _sc_syms_print(sc_sym *s, int off, char _filled[1024]) {
    int i;
    for (i = 0; i < off; i++) printf(_filled[i] ? "\xBA " : "  ");

    {
        int l = (int)strlen(s->id);
        if (strcmp(s->id, s->value) != 0) l += 2 + strlen(s->value);
        int n = (s->first != NULL) + (s->second != NULL) + (s->third != NULL);

        printf("\xBA\xC9");
        for (i = 0; i < l + n * 2; i++) printf("\xCD");
        printf("\xBB\n");
        for (i = 0; i < off; i++) printf(_filled[i] ? "\xBA " : "  ");

        if (strcmp(s->id, s->value) != 0) {
            if (n == 0) printf("\xC8\xB9%s, %s\xBA\n", s->id, s->value);
            else if (n == 1) printf("\xC8\xB9 %s, %s \xBA\n", s->id, s->value);
            else if (n == 2) printf("\xC8\xB9  %s, %s  \xBA\n", s->id, s->value);
            else if (n == 3) printf("\xC8\xB9   %s, %s   \xBA\n", s->id, s->value);
        } else {
            if (n == 0) printf("\xC8\xB9%s\xBA\n", s->id);
            else if (n == 1) printf("\xC8\xB9 %s \xBA\n", s->id);
            else if (n == 2) printf("\xC8\xB9  %s  \xBA\n", s->id);
            else if (n == 3) printf("\xC8\xB9   %s   \xBA\n", s->id);
        }

        for (i = 0; i < off; i++) printf(_filled[i] ? "\xBA " : "  ");
        printf(" \xC8");
        for (i = 0; i < n; i++) printf("\xCB\xCD");
        for (i = 0; i < l; i++) printf("\xCD");
        printf("\xBC\n");
    }

    _filled[off] = 0;

    if (s->first) _filled[++off] = 1;
    if (s->second) _filled[++off] = 1;
    if (s->third) _filled[++off] = 1;

    if (s->third) {
        _sc_syms_print(s->third, off, _filled);
        off--;
    }

    if (s->second) {
        _sc_syms_print(s->second, off, _filled);
        off--;
    }

    if (s->first) {
        _sc_syms_print(s->first, off,_filled);
        off--;
    }
}

void sc_sym_print(sc_sym *s) {
    static char _filled[1024];
    _sc_syms_print(s, 0, _filled);
}

void sc_syms_print(sc_syms *s) {
    static char _filled[1024];
    sc_syms_blck *cur_blck = s->head;
    while (cur_blck) {
        int i;
        for (i = 0; i < cur_blck->len; i++) {
            _sc_syms_print(&cur_blck->syms[i], 0, _filled);
        }
        cur_blck = cur_blck->next;
    }
}