
#include <stdio.h>

void test_str_pool();
void test_tok();
void test_par();
void test_calc();
void test_vm();

int main() {
    test_str_pool();
    test_tok();
	test_par();
    test_calc();
    test_vm();
    puts("OK");
    return 0;
}

