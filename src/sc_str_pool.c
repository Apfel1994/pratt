#include "sc_str_pool.h"

#include <stdlib.h>
#include <string.h>

#ifndef max
#define max(a,b) ((a) > (b) ? (a) : (b))
#endif

typedef struct sc_str_pool_blck {
    char *buf;
    int len, cap;
    struct sc_str_pool_blck *parent;
} sc_str_pool_blck;

void sc_str_pool_init(sc_str_pool *p) {
    sc_str_pool_blck *t = malloc(sizeof(sc_str_pool_blck));
    t->cap = 128;
    t->len = 0;
    t->buf = malloc((size_t)t->cap);
    t->parent = NULL;
    p->tail = t;
}

void sc_str_pool_destroy(sc_str_pool *p) {
    sc_str_pool_blck *cur_blck = p->tail;
    while (cur_blck) {
        sc_str_pool_blck *next = cur_blck->parent;
        free(cur_blck->buf);
        free(cur_blck);
        cur_blck = next;
    }
}

char *sc_str_pool_alloc(sc_str_pool *p, const char *s, int len) {
    sc_str_pool_blck *t = p->tail;
    if (!len) len = (int)strlen(s);
    if (t->len + len + 1 > t->cap) {
        sc_str_pool_blck *new_blck = malloc(sizeof(sc_str_pool_blck));
        new_blck->cap = max(t->cap * 2, len * 2);
        new_blck->len = 0;
        new_blck->buf = malloc((size_t)new_blck->cap);
        new_blck->parent = t;
        t = p->tail = new_blck;
    }

    char *ret = &t->buf[t->len];
    memcpy(ret, s, (size_t)len);
    t->buf[t->len + len] = '\0';
    t->len += (len + 1);
    return ret;
}