#include "sc_scope.h"

#include <stdlib.h>

struct sc_par_state;

static sc_sym *_sc_nud_itself(struct sc_par_state *state, sc_sym *self) {
    return self;
}

void sc_scope_init(sc_scope *scope) {
    sc_syms_init(&scope->defs);
    scope->parent = NULL;
}

void sc_scope_destroy(sc_scope *scope) {
    sc_syms_destroy(&scope->defs);
    scope->parent = NULL;
}

sc_sym *sc_scope_define(sc_scope *scope, sc_sym sym) {
    sc_sym *t = sc_syms_find(&scope->defs, NULL, sym.value);
    if (t) {
        // TODO: error!!!
    }

    t = sc_syms_push(&scope->defs, sym);
    t->nud = _sc_nud_itself;
    t->led = NULL;
    t->lbp = 0;
    t->scope = scope;
    return t;
}

sc_sym *sc_scope_find(sc_scope *scope, const char *id, const char *val) {
    sc_scope *cur_scope = scope;
    while (cur_scope) {
        sc_sym *t = sc_syms_find(&cur_scope->defs, id, val);
        if (t) {
            return t;
        }
        cur_scope = cur_scope->parent;
    }
    return NULL;
}

sc_sym *sc_scope_reserve(sc_scope *scope, sc_sym sym) {
    if (sym.arity != AR_NAME || sym.reserved) {
        return NULL;
    }
    sc_sym *t = sc_syms_find(&scope->defs, NULL, sym.value);
    if (t) {
        if (t->reserved) {
            return NULL;
        }
        if (t->arity == AR_NAME) {
            sym.error = "Already defined";
        }
    }
    t = sc_syms_push(&scope->defs, sym);
    t->reserved = 1;

	return t;
}