#include "test_assert.h"

#include <math.h>
#include <string.h>

#include "sc/sc.h"

void sc_include_codes_const(sc_vm* vm, const char* name, const sc_code* codes);

static void require_eq_approx(double v1, double v2, double d) {
    require(fabs(v1 - v2) < d);
}

void test_vm() {
    {   /* test number */
        sc_vm *vm = sc_new_vm();

        double n1 = 8.654, n2 = 3.245;
        sc_code *p_n1 = (sc_code *)&n1,
                *p_n2 = (sc_code *)&n2;

        sc_code codes[] = { { SC_INUM, 0, 0, 0 }, p_n1[0], p_n1[1],
                            { SC_INUM, 1, 0, 0 }, p_n2[0], p_n2[1],
                            { SC_IADD, 2, 0, 1 },
                            { SC_IEOF, 0, 0, 0 } };

        sc_include_codes_const(vm, "test", codes);

        require(vm->regs[0].type == SC_NUM);
        require_eq_approx(vm->regs[0].number.val, n1, 0.000001);
        require(vm->regs[1].type == SC_NUM);
        require_eq_approx(vm->regs[1].number.val, n2, 0.000001);
        require(vm->regs[2].type == SC_NUM);
        require_eq_approx(vm->regs[2].number.val, (n1 + n2), 0.000001);

        sc_deinit_vm(vm);
    }

    {   /* test string */
        sc_vm *vm = sc_new_vm();

        const char *str1 = "string1\0",
                   *str2 = "string2\0";
        const char *sum = "string1string2";
        sc_code *p_str1 = (sc_code *)str1,
                *p_str2 = (sc_code *)str2;

        unsigned short len1 = (unsigned short)strlen(str1),
                       len2 = (unsigned short)strlen(str2);
        unsigned char *p_len1 = (unsigned char *)&len1,
                      *p_len2 = (unsigned char *)&len2;

        sc_code codes[] = { { SC_ISTR, 0, p_len1[0], p_len1[1] }, p_str1[0], p_str1[1],
                            { SC_ISTR, 1, p_len2[0], p_len2[1] }, p_str2[0], p_str2[1],
                            { SC_IADD, 2, 0, 1 },
                            { SC_IEOF, 0, 0, 0 } };

        sc_include_codes_const(vm, "test", codes);

        require(vm->regs[0].type == SC_STR);
        require(strcmp(vm->regs[0].string.val, str1) == 0);
        require(vm->regs[1].type == SC_STR);
        require(strcmp(vm->regs[1].string.val, str2) == 0);
        require(vm->regs[2].type == SC_STR);
        require(strcmp(vm->regs[2].string.val, sum) == 0);

        sc_deinit_vm(vm);
    }

    {   /* test vector */
        sc_vm *vm = sc_new_vm();

        double n1 = 8.654;
        sc_code *p_n1 = (sc_code *)&n1;

        const char *str1 = "string1\0";
        sc_code *p_str1 = (sc_code *)str1;

        unsigned short len1 = (unsigned short)strlen(str1);
        unsigned char *p_len1 = (unsigned char *)&len1;

        sc_code codes[] = { { SC_INUM, 0, 0, 0 }, p_n1[0], p_n1[1],
                            { SC_ISTR, 1, p_len1[0], p_len1[1] }, p_str1[0], p_str1[1],
                            { SC_IVEC, 2, 2, 0 },
                            { SC_IEOF, 0, 0, 0 } };

        sc_include_codes_const(vm, "test", codes);

        require(vm->regs[0].type == SC_NUM);
        require_eq_approx(vm->regs[0].number.val, n1, 0.000001);
        require(vm->regs[1].type == SC_STR);
        require(strcmp(vm->regs[1].string.val, str1) == 0);
        require(vm->regs[2].type == SC_VEC);
        require(vm->regs[2].vector.val->size == 2);
        require(vm->regs[2].vector.val->items[0].type == SC_NUM);
        require_eq_approx(vm->regs[2].vector.val->items[0].number.val, n1, 0.000001);
        require(vm->regs[2].vector.val->items[1].type == SC_STR);
        require(strcmp(vm->regs[2].vector.val->items[1].string.val, str1) == 0);
        require(vm->regs[2].vector.val->items[1].string.val == vm->regs[1].string.val);

        sc_deinit_vm(vm);
    }

    {   /* test map */
        sc_vm *vm = sc_new_vm();

        double n1 = 8.654, n2 = 3.245;
        sc_code *p_n1 = (sc_code *)&n1,
                *p_n2 = (sc_code *)&n2;

        const char *str1 = "string1\0",
                   *str2 = "string2\0";
        sc_code *p_str1 = (sc_code *)str1,
                *p_str2 = (sc_code *)str2;

        unsigned short len1 = (unsigned short)strlen(str1),
                       len2 = (unsigned short)strlen(str2);
        unsigned char *p_len1 = (unsigned char *)&len1,
                      *p_len2 = (unsigned char *)&len2;

        sc_code codes[] = { { SC_INUM, 0, 0, 0 }, p_n1[0], p_n1[1],
                            { SC_ISTR, 1, p_len1[0], p_len1[1] }, p_str1[0], p_str1[1],
                            { SC_ISTR, 2, p_len2[0], p_len2[1] }, p_str2[0], p_str2[1],
                            { SC_INUM, 3, 0, 0 }, p_n2[0], p_n2[1],
                            { SC_IMAP, 4, 2, 0 },
                            { SC_IEOF, 0, 0, 0 } };

        sc_include_codes_const(vm, "test", codes);

        require(vm->regs[0].type == SC_NUM);
        require_eq_approx(vm->regs[0].number.val, n1, 0.000001);
        require(vm->regs[1].type == SC_STR);
        require(strcmp(vm->regs[1].string.val, str1) == 0);
        require(vm->regs[2].type == SC_STR);
        require(strcmp(vm->regs[2].string.val, str2) == 0);
        require(vm->regs[3].type == SC_NUM);
        require_eq_approx(vm->regs[3].number.val, n2, 0.000001);
        require(vm->regs[4].type == SC_MAP);
        require(vm->regs[4].map.val->size == 2);

        sc_obj obj1 = sc_get(vm, vm->regs[4], vm->regs[0]),
               obj2 = sc_get(vm, vm->regs[4], vm->regs[2]);
        require(obj1.type == SC_STR);
        require(strcmp(obj1.string.val, str1) == 0);
        require(obj2.type == SC_NUM);
        require_eq_approx(obj2.number.val, n2, 0.000001);

        sc_deinit_vm(vm);
    }
}