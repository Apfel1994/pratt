#include "sc_tok.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void sc_toks_init(sc_toks *t) {
    t->toks = malloc(64 * sizeof(sc_tok));
    t->len = 0;
    t->cap = 64;
    sc_str_pool_init(&t->str_pool);
}

void sc_toks_destroy(sc_toks *t) {
    free(t->toks);
    t->toks = NULL;
    t->len = t->cap = 0;
    sc_str_pool_destroy(&t->str_pool);
}

void sc_toks_push(sc_toks *toks, sc_tok tok) {
    if (toks->len == toks->cap) {
        toks->cap *= 2;
        toks->toks = realloc(toks->toks, toks->cap * sizeof(sc_tok));
    }
    toks->toks[toks->len++] = tok;
}

int sc_tokenize(const char *src, sc_toks *toks) {
    const char *prefix = "<>+-&", *suffix = "=>&:";
    int i, length = (int)strlen(src);
    
    assert(toks->toks == NULL);
    sc_toks_init(toks);

    for (i = 0; i < length; ) {
        int from = i;
        char c = src[i];

        if (c == ' ') {
			i++;
            continue;
        } else if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || c == '_') { // name
            i++;
            while (1) {
                c = src[i];
                if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') ||
                    (c >= '0' && c <= '9') || c == '_') {
                    i++;
                } else {
                    break;
                }
            }
            sc_tok str = { TOK_NAME, sc_str_pool_alloc(&toks->str_pool, &src[from], i - from) };
            sc_toks_push(toks, str);
        } else if (c >= '0' && c <= '9') { // number
            i++;
            while (1) {
                c = src[i];
                if (c < '0' || c > '9') {
                    break;
                }
                i++;
            }

            if (c == '.') {
                i++;
                while (1) {
                    c = src[i];
                    if (c < '0' || c > '9') {
                        break;
                    }
                    i++;
                }
            }

            if (c == 'e' || c == 'E') {
                i++;
                c = src[i];
                if (c == '-' || c == '+') {
                    i++;
                    c = src[i];
                }
                if (c < '0' || c > '9') {
                    sc_tok num = { TOK_NUMBER, sc_str_pool_alloc(&toks->str_pool, &src[from], i - from) };
                    num.error = "Bad exponent";
                    sc_toks_push(toks, num);
                    return -1;
                }
                do {
                    i++;
                    c = src[i];
                } while (c >= '0' && c <= '9');
            }

            if ((c >= 'a' && c <= 'z') || c == '.') {
                i++;
                sc_tok num = { TOK_NUMBER, sc_str_pool_alloc(&toks->str_pool, &src[from], i - from), "Bad number" };
                sc_toks_push(toks, num);
                return -1;
            }

            sc_tok num = { TOK_NUMBER, sc_str_pool_alloc(&toks->str_pool, &src[from], i - from) };
            sc_toks_push(toks, num);
        } else if (c == '\'' || c == '"') { // string
            char q = c;
            i++;
            while (1) {
                c = src[i];
                if (c < ' ') {
                    sc_tok str = { TOK_STRING, sc_str_pool_alloc(&toks->str_pool, &src[from], i - from) };
                    str.error = (c == '\n' || c == '\r' || c == 0) ?
                        "Unterminated string." :
                        "Control character in string.";
                    sc_toks_push(toks, str);
                    return -1;
                }

                if (c == q) {
                    break;
                }

                i++;
            }

			// TODO: escapement

            sc_tok str = { TOK_STRING, sc_str_pool_alloc(&toks->str_pool, &src[from + 1], i - from - 1) };
            sc_toks_push(toks, str);
			i++;
        } else if (c == '/' && src[i + 1] == '/') { // comment
            i++;
            while (1) {
                c = src[i];
                if (c == '\n' || c == '\r' || c == 0) {
                    break;
                }
                i++;
            }
        } else if (strchr(prefix, c)) { // combining
			i++;
            while (1) {
                c = src[i];
                if (i >= length || !strchr(suffix, c)) {
                    break;
                }
                i++;
            }
            sc_tok op = { TOK_OPERATOR, sc_str_pool_alloc(&toks->str_pool, &src[from], i - from) };
            sc_toks_push(toks, op);
        } else { // single-character operator
			i++;
            sc_tok op = { TOK_OPERATOR, sc_str_pool_alloc(&toks->str_pool, &src[from], 1) };
            sc_toks_push(toks, op);
        }
    }
    
    return 0;
}

void sc_toks_print(sc_toks *t) {
    int i;
    for (i = 0; i < t->len; i++) {
        sc_tok *tok = &t->toks[i];
        if (tok->type == TOK_NAME) {
            printf("NAME:     ");
        } else if (tok->type == TOK_NUMBER) {
            printf("NUMBER:   ");
        } else if (tok->type == TOK_STRING) {
            printf("STRING:   ");
        } else if (tok->type == TOK_OPERATOR) {
            printf("OPERATOR: ");
        }
        printf("\"%s\"\n", t->toks[i].val);
    }
}