#include "sc.h"

sc_obj sc_number(double val) {
    sc_obj o = { SC_NUM };
    o.number.val = val;
    return o;
}