#include "sc.h"

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

/* sc_gc.c */
sc_obj sc_gc_track(sc_vm* vm, sc_obj o);
void sc_add_to_grey(sc_vm* vm, sc_obj v);

/* sc_vec.c */
sc_obj _sc_vector_get(sc_vm* vm, _sc_vector* vec, int k);
void _sc_vector_set(sc_vm* vm, _sc_vector* vec, int k, sc_obj v);
void _sc_vector_push_back(sc_vm* vm, _sc_vector* vec, sc_obj v);
sc_obj _sc_vector_pop(sc_vm* vm, _sc_vector* vec, int k);
sc_obj sc_push_back(sc_vm *vm);

/* sc_map.c */
sc_obj _sc_map_get(sc_vm* vm, _sc_map* map, sc_obj k);
void _sc_map_set(sc_vm* vm, _sc_map* map, sc_obj k, sc_obj v);
int _sc_map_hash_find(sc_vm* vm, _sc_map* map, int hash, sc_obj k);

/* sc_func.c */
sc_obj sc_func(sc_vm* vm, int type, void* jmp, sc_obj const_pars, sc_obj globals);

sc_obj sc_add(sc_vm* vm, sc_obj a, sc_obj b) {
    if (a.type == SC_NUM && b.type == SC_NUM) {
        return sc_number(a.number.val + b.number.val);
    } else if (a.type == SC_STR && b.type == SC_STR) {
        int a_len = a.string.size, b_len = b.string.size;
        sc_obj o = sc_string_t(vm, a_len + b_len);
        char* s = o.string.str->s;
        memcpy(s, a.string.val, a_len);
        memcpy(s + a_len, b.string.val, b_len);
        return sc_gc_track(vm, o);
    }
    return sc_null;
}

sc_obj sc_sub(sc_vm* vm, sc_obj a, sc_obj b) {
    if (a.type == SC_NUM && b.type == SC_NUM) {
        return sc_number(a.number.val - b.number.val);
    }
    return sc_null;
}

sc_obj sc_mul(sc_vm* vm, sc_obj a, sc_obj b) {
    if (a.type == SC_NUM && b.type == SC_NUM) {
        return sc_number(a.number.val * b.number.val);
    }
    return sc_null;
}

sc_obj sc_div(sc_obj a, sc_obj b) {
    return sc_number(a.number.val / b.number.val);
}

#define sc_min(a, b) a < b ? a : b

int sc_cmp(sc_vm* vm, sc_obj a, sc_obj b) {
    if (a.type != b.type)
        return (a.type - b.type);
    switch (a.type) {
    case SC_NUM: {
        double dif = a.number.val - b.number.val;
        if (dif < 0.0) {
            return -1;
        } else {
            if (dif > 0.0) {
                return 1;
            } else {
                return 0;
            }
        }
    }
    case SC_STR: {
        int v = memcmp(a.string.val, b.string.val, sc_min(a.string.size, b.string.size));
        if (v == 0) {
            return (a.string.size - b.string.size);
        }
        return v;
    }
    case SC_VEC: {
        int i, v;
        v = a.vector.val->size - b.vector.val->size;
        if (!v) {
            for (i = 0; i < sc_min(a.vector.val->size, b.vector.val->size); i++) {
                sc_obj aa = a.vector.val->items[i], bb = b.vector.val->items[i];
                if (aa.type == SC_VEC && bb.type == SC_VEC) {
                    v = aa.vector.val - bb.vector.val;
                } else {
                    v = sc_cmp(vm, aa, bb);
                }
                if (v) {
                    return v;
                }
            }
            return 0;
        } else {
            return v;
        }
    }
    case SC_MAP:
        return (a.map.val - b.map.val);
    case SC_FUNC:
        return (a.func.val - b.func.val);
    case SC_NULL:
        return 1;
    case SC_CDATA:
        return ((char*)a.cdata.val - (char*)a.cdata.val);
    }
    /*TODO ���-�� ����� ����������*/
    assert(0);
    return -1;
}

int sc_bool(sc_vm* vm, sc_obj a) {
    switch (a.type) {
    case SC_NULL:
        return 0;
        break;
    case SC_NUM:
        return a.number.val != 0;
        break;
    case SC_STR:
        return a.string.size != 0;
        break;
    }
    return 0;
}

sc_obj sc_get(sc_vm* vm, sc_obj obj, sc_obj k) {
    if (obj.type == SC_VEC) {
        if (k.type == SC_NUM) {
            return _sc_vector_get(vm, obj.vector.val, (int)k.number.val);
        } else if (k.type == SC_STR) {
            if (strcmp(k.string.val, "push_back") == 0) {
                sc_obj par = sc_vector_n(vm, 1, &obj);
                return sc_func(vm, 1, &sc_push_back, par, sc_null);
            } else if (strcmp(k.string.val, "size") == 0) {
                return sc_number(obj.vector.val->size);
            }
        } else if (k.type == SC_NULL) {
            return _sc_vector_pop(vm, obj.vector.val, 0);
        }
    } else if (obj.type == SC_STR) {
        if (k.type == SC_NUM) {
            int len = obj.string.size;
            int i = (int)k.number.val;
            int index = (i < 0 ? len + i : i);
            return sc_string((char*)vm->chars[(unsigned char)obj.string.val[index]], 1);
        }
    } else if (obj.type == SC_MAP) {
        return _sc_map_get(vm, obj.map.val, k);
    }
    return sc_null;
}

static int _lua_hash(const void *v, int l) {
    int i, step = (l >> 5) + 1;
    int h = l + (l >= 4 ? *(int*)v : 0);
    for (i = l; i >= step; i -= step) {
        h = h ^ ((h << 5) + (h >> 2) + ((unsigned char *)v)[i - 1]);
    }
    return h;
}

int sc_hash(sc_vm* vm, sc_obj obj) {
    int hash, i;
    switch (obj.type) {
    case SC_NULL:
        return 0;
    case SC_NUM:
        return _lua_hash(&obj.number.val, sizeof(double));
    case SC_STR:
        return _lua_hash(obj.string.val, obj.string.size);
    case SC_VEC:
        hash = obj.vector.val->size;
        for (i = 0; i < obj.vector.val->size; i++) {
            sc_obj item = obj.vector.val->items[i];
            if (item.type == SC_VEC) {
                hash += _lua_hash(&item.vector.val, sizeof(void*));
            } else {
                hash += sc_hash(vm, item);
            }
        }
        return hash;
    case SC_MAP:
        return _lua_hash(&obj.map.val, sizeof(void*));
    case SC_FUNC:
        return _lua_hash(&obj.func.val, sizeof(void*));
    case SC_CDATA:
        return _lua_hash(&obj.cdata.ptr, sizeof(void*));
    }
    return 0;
}


int sc_iget(sc_vm* vm, sc_obj* ret, sc_obj obj, sc_obj k) {
    int i;
    if (obj.type == SC_MAP) {
        i = _sc_map_hash_find(vm, obj.map.val, sc_hash(vm, k), k);
        if (i < 0) {
            return 0;
        } else {
            *ret = obj.map.val->items[i].val;
            return 1;
        }
    } else if (obj.type == SC_VEC) {
        if (!obj.vector.val->size) {
            return 0;
        } else {
            *ret = sc_get(vm, obj, k);
            sc_add_to_grey(vm, *ret);
        }
    }
    return 0;
}

void sc_set(sc_vm* vm, sc_obj obj, sc_obj k, sc_obj v) {
    if (obj.type == SC_VEC) {
        if (k.type == SC_NUM) {
            _sc_vector_set(vm, obj.vector.val, (int)k.number.val, v);
            return;
        } else if (k.type == SC_NULL) {
            _sc_vector_push_back(vm, obj.vector.val, v);
            return;
        }
    } else if (obj.type == SC_MAP) {
        _sc_map_set(vm, obj.map.val, k, v);
        return;
    } else if (obj.type == SC_FUNC) {
        if (k.type == SC_NULL) {
            obj.func.val->const_pars = v;
            obj.func.val->type = 2;
        }
    } else {
        printf("Wrong type for 'SET'\n");
        assert(0);
    }
}

sc_obj sc_to_string(sc_vm* vm, sc_obj obj) {
    if (obj.type == SC_NUM) {
        double v = obj.number.val;
        if (fabs(fabs(v) - fabs(round(v))) > 0.000001) {
            return sc_string_f(vm, "%9.16f", v);
        }
        return sc_string_f(vm, "%ld", (long)round(v));
    } else if (obj.type == SC_STR) {
        return obj;
    } else if (obj.type == SC_VEC) {
        _sc_vector* v = obj.vector.val;
        return sc_string_f(vm, "<Vector> 0x%x, size = %d, cap = %d", v, v->size, v->capacity);
    } else if (obj.type == SC_MAP) {
        _sc_map* v = obj.map.val;
        return sc_string_f(vm, "<Map> 0x%x, size = %d, cap = %d", v, v->size, v->capacity);
    } else if (obj.type == SC_FUNC) {
        _sc_func* v = obj.func.val;
        return sc_string_f(vm, "<Func> 0x%x, type = %d", v, v->type);
    } else if (obj.type == SC_CDATA) {
        _sc_cdata* v = obj.cdata.val;
        return sc_string_f(vm, "<Cdata> 0x%x", v);
    }
    return sc_string_f(vm, "<?>");
}

sc_obj sc_print(sc_vm* vm) {
    int l = vm->cur_params_vec.vector.val->size, i;
    sc_obj par;
    for (i = 0; i < l; i++) {
        par = _sc_vector_get(vm, vm->cur_params_vec.vector.val, i);
        if (i) {
            printf(" ");
        }
        printf("%s", (sc_to_string(vm, par)).string.val);
    }
    printf("\n");
    return sc_null;
}
