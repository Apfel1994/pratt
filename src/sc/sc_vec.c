#include "sc.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* sc_gc.c */
sc_obj sc_gc_track(sc_vm* vm, sc_obj o);
void sc_add_to_grey(sc_vm* vm, sc_obj v);

/* ??? */
int sc_cmp(sc_vm* vm, sc_obj a, sc_obj b);

_sc_vector* _sc_vector_new() {
    return (_sc_vector*)calloc(1, sizeof(_sc_vector));
}

void _sc_vector_free(_sc_vector* vec) {
    free(vec->items);
    free(vec);
}

void _sc_vector_resize(_sc_vector* vec, int size) {
    if (size == 0) {
        size = 1;
    }
    vec->items = (sc_obj*)realloc(vec->items, size * sizeof(sc_obj));
    vec->capacity = size;
}

void _sc_vector_set(sc_vm* vm, _sc_vector* vec, int k, sc_obj v) {
    if (k >= vec->size) {
        printf("index > len");
        return;
    }
    vec->items[k] = v;
    sc_add_to_grey(vm, v);
}

sc_obj _sc_vector_get(sc_vm* vm, _sc_vector* vec, int k) {
    sc_obj o = { SC_NULL };
    if (k >= vec->size) {
        printf("index > size");
        return o;
    }
    o = vec->items[k];
    return o;
}

void _sc_vector_insert_nogrey(sc_vm* vm, _sc_vector* vec, int k, sc_obj v) {
    if (vec->size == vec->capacity) {
        _sc_vector_resize(vec, vec->size * 2);
    }
    if (k < vec->size) {
        memmove(&vec->items[k + 1], &vec->items[k], sizeof(sc_obj) * (vec->size - k));
    }
    vec->items[k] = v;
    vec->size++;
}

void _sc_vector_push_back_nogrey(sc_vm* vm, _sc_vector* vec, sc_obj v) {
    _sc_vector_insert_nogrey(vm, vec, vec->size, v);
}

void _sc_vector_insert(sc_vm* vm, _sc_vector* vec, int k, sc_obj v) {
    _sc_vector_insert_nogrey(vm, vec, k, v);
    sc_add_to_grey(vm, v);
}

void _sc_vector_push_back(sc_vm* vm, _sc_vector* vec, sc_obj v) {
    _sc_vector_push_back_nogrey(vm, vec, v);
    sc_add_to_grey(vm, v);
}

sc_obj _sc_vector_pop(sc_vm* vm, _sc_vector* vec, int k) {
    sc_obj o = _sc_vector_get(vm, vec, k);
    if (k != vec->size - 1) {
        memmove(&vec->items[k], &vec->items[k + 1], sizeof(sc_obj) * (vec->size - (k + 1)));
    }
    vec->size--;
    return o;
}

void _sc_vector_shrink_to_fit(sc_vm* vm, _sc_vector* vec) {
    if (vec->capacity != vec->size) {
        _sc_vector_resize(vec, vec->size);
    }
}

int _sc_vector_find(sc_vm* vm, _sc_vector* vec, sc_obj k) {
    int i;
    for (i = 0; i < vec->size; i++) {
        if (sc_cmp(vm, vec->items[i], k) == 0) {
            return i;
        }
    }
    return -1;
}

sc_obj sc_vector(sc_vm* vm) {
    sc_obj o = { SC_VEC };
    o.vector.val = _sc_vector_new();
    if (vm) {
        return sc_gc_track(vm, o);
    } else {
        return o;
    }
}

sc_obj sc_vector_n(sc_vm* vm, int size, sc_obj* objs) {
    int i;
    sc_obj o = sc_vector(vm);
    _sc_vector_resize(o.vector.val, size);
    for (i = 0; i < size; i++) {
        _sc_vector_push_back(vm, o.vector.val, objs[i]);
    }
    return o;
}

sc_obj sc_vector_v(sc_vm* vm, int n, ...) {
    int i;
    sc_obj r = sc_vector(vm);
    va_list a;
    va_start(a, n);
    for (i = 0; i < n; i++) {
        _sc_vector_push_back(vm, r.vector.val, va_arg(a, sc_obj));
    }
    va_end(a);
    return r;
}

sc_obj sc_push_back(sc_vm *vm) {
    sc_obj self = sc_get(vm, vm->cur_params_vec, sc_null);
    sc_obj v = sc_get(vm, vm->cur_params_vec, sc_null);
    _sc_vector_push_back(vm, self.vector.val, v);
    return sc_null;
}