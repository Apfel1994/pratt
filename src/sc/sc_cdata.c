#include "sc.h"

#include <stdlib.h>

/* sc_gc.c */
sc_obj sc_gc_track(sc_vm* vm, sc_obj o);

sc_obj sc_cdata(sc_vm* vm, void* ptr, void* free_func) {
    sc_obj ret = { SC_CDATA };
    ret.cdata.val = (_sc_cdata*)calloc(1, sizeof(_sc_cdata));
    ret.cdata.ptr = ptr;
    ret.cdata.val->p_free = free_func;
    return sc_gc_track(vm, ret);
}