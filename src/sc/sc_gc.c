#include "sc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* sc_vec.c */
void _sc_vector_free(_sc_vector* vec);
void _sc_vector_push_back(sc_vm* vm, _sc_vector* vec, sc_obj v);
sc_obj _sc_vector_pop(sc_vm* vm, _sc_vector* vec, int k);
void _sc_vector_push_back_nogrey(sc_vm* vm, _sc_vector* vec, sc_obj v);

/* sc_map.c */
void _sc_map_free(_sc_map* map);
int _sc_map_next(sc_vm* vm, _sc_map* map);

void sc_add_to_grey(sc_vm* vm, sc_obj v) {
    if (v.type < SC_STR || (!v.gc.p_mark) || *v.gc.p_mark) {
        return;
    }
    *v.gc.p_mark = 1;
    if (v.type == SC_STR || v.type == SC_CDATA) {
        _sc_vector_push_back_nogrey(vm, vm->black, v);
        return;
    }
    _sc_vector_push_back_nogrey(vm, vm->grey, v);
}

void sc_follow(sc_vm* vm, sc_obj v) {
    int i;
    if (v.type == SC_VEC) {
        for (i = 0; i < v.vector.val->size; i++) {
            sc_add_to_grey(vm, v.vector.val->items[i]);
        }
    } else if (v.type == SC_MAP) {
        for (i = 0; i < v.map.val->size; i++) {
            int ndx = _sc_map_next(vm, v.map.val);
            sc_add_to_grey(vm, v.map.val->items[ndx].key);
            sc_add_to_grey(vm, v.map.val->items[ndx].val);
        }

    } else if (v.type == SC_FUNC) {
        sc_add_to_grey(vm, v.func.val->globals);
    }
}

void sc_gc_reset(sc_vm* vm) {
    _sc_vector* tmp;
    int i;
    for (i = 0; i < vm->black->size; i++) {
        *(vm->black->items[i].gc.p_mark) = 0;
    }
    tmp = vm->black;
    vm->black = vm->white;
    vm->white = tmp;
}

void sc_delete(sc_vm* vm, sc_obj o) {
    switch (o.type) {
    case SC_STR:
        free(o.string.str);
        break;
    case SC_VEC:
        _sc_vector_free(o.vector.val);
        break;
    case SC_MAP:
        _sc_map_free(o.map.val);
        break;
    case SC_FUNC:
        free(o.func.val);
        break;
    case SC_CDATA:
        if (o.cdata.val->p_free) {
            o.cdata.val->p_free(vm, o);
        }
        free(o.cdata.val);
        break;
    }
}

void sc_gc_collect(sc_vm* vm) {
    int i;
    //printf("sc_gc_collect\n");
    for (i = 0; i < vm->white->size; i++) {
        sc_obj item = vm->white->items[i];
        if (!*item.gc.p_mark) {
            sc_delete(vm, item);
        }
    }
    vm->white->size = 0;
    sc_gc_reset(vm);
}

void sc_gc_process_grey(sc_vm* vm) {
    sc_obj o;
    if (!vm->grey->size) {
        return;
    }
    o = _sc_vector_pop(vm, vm->grey, vm->grey->size - 1);
    sc_follow(vm, o);
    _sc_vector_push_back(vm, vm->black, o);
}

void sc_full(sc_vm* vm) {
    while (vm->grey->size) {
        sc_gc_process_grey(vm);
    }
    sc_gc_collect(vm);
    sc_follow(vm, vm->root);
}

void sc_gc_step(sc_vm* vm) {
    vm->gc_steps++;
    if (vm->gc_steps < 4096 || vm->grey->size > 0) {
        sc_gc_process_grey(vm);
        sc_gc_process_grey(vm);
    }
    if (vm->gc_steps < 4096 || vm->grey->size > 0) {
        return;
    }
    vm->gc_steps = 0;
    sc_full(vm);
}

sc_obj sc_gc_track(sc_vm* vm, sc_obj o) {
    sc_gc_step(vm);
    sc_add_to_grey(vm, o);
    return o;
}