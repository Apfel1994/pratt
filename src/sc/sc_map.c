﻿#include "sc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* sc_gc.c */
sc_obj sc_gc_track(sc_vm* vm, sc_obj o);
void sc_add_to_grey(sc_vm* vm, sc_obj v);

/* ??? */
int sc_cmp(sc_vm* vm, sc_obj a, sc_obj b);
int sc_hash(sc_vm* vm, sc_obj obj);

_sc_map* _sc_map_new() {
    return (_sc_map*)calloc(1, sizeof(_sc_map));
}

void _sc_map_free(_sc_map* map) {
    free(map->items);
    free(map);
}

void _sc_map_hash_set(_sc_map* map, int hash, sc_obj k, sc_obj v) {
    int i;
    int index = hash & map->mask;
    for (i = index; i < index + map->capacity; i++) {
        int n = i & map->mask;
        if (map->items[n].used > 0) {
            continue;
        }
        if (map->items[n].used == 0) {
            map->num_non_empty++;
        }
        map->items[n].used = 1;
        map->items[n].hash = hash;
        map->items[n].key = k;
        map->items[n].val = v;
        map->size++;
        return;
    }
}

void _sc_map_resize(_sc_map* map, int new_size) {
    int i, old_cap;
    _sc_map_item* old_items = map->items;
    new_size = new_size < 8 ? 8 : new_size;
    map->items = (_sc_map_item*)calloc(new_size, sizeof(_sc_map_item));
    old_cap = map->capacity;
    map->capacity = new_size;
    map->mask = new_size - 1;
    map->size = 0;
    map->num_non_empty = 0;
    for (i = 0; i < old_cap; i++) {
        if (old_items[i].used == 1) {
            _sc_map_hash_set(map, old_items[i].hash, old_items[i].key, old_items[i].val);
        }
    }
    free(old_items);
}

int _sc_map_hash_find(sc_vm* vm, _sc_map* map, int hash, sc_obj k) {
    int i;
    int index = hash & map->mask;
    for (i = index; i < index + map->capacity; i++) {
        int n = i & map->mask;
        if (map->items[n].used == 0) {
            break;
        }
        if (map->items[n].used < 0) {
            continue;
        }
        if (map->items[n].hash != hash) {
            continue;
        }
        if (sc_cmp(vm, k, map->items[n].key) != 0) {
            continue;
        }
        return n;
    }
    return -1;
}

void _sc_map_flush_unused(_sc_map* map) {
    int i;
    for (i = 0; i < map->capacity; i++) {
        if (map->items[i].used == -1) {
            map->items[i].used = 0;
            map->items[i].hash = 0;
            map->items[i].key = sc_null;
            map->items[i].val = sc_null;
        }
    }
    map->num_non_empty = map->size;
}

void _sc_map_set_nogrey(sc_vm* vm, _sc_map* map, sc_obj k, sc_obj v) {
    int hash = sc_hash(vm, k);
    int n = _sc_map_hash_find(vm, map, hash, k);
    if (n == -1) {
        if (map->size >= map->capacity / 2) {
            _sc_map_resize(map, map->capacity * 2);
        } else if (map->num_non_empty >= map->capacity * 3 / 4) {
            _sc_map_flush_unused(map);
        }
        _sc_map_hash_set(map, hash, k, v);
        n = _sc_map_hash_find(vm, map, hash, k);
    } else {
        map->items[n].val = v;
    }
}

void _sc_map_set(sc_vm* vm, _sc_map* map, sc_obj k, sc_obj v) {
    _sc_map_set_nogrey(vm, map, k, v);
    sc_add_to_grey(vm, k);
    sc_add_to_grey(vm, v);
}

sc_obj _sc_map_get(sc_vm* vm, _sc_map* map, sc_obj k) {
    int ndx = _sc_map_hash_find(vm, map, sc_hash(vm, k), k);
    if (ndx < 0) {
        printf("ndx < 0 key: %s\n", sc_to_string(vm, k).string.val);
        /* TODO ���-������ ���� ���������� ����*/
    }
    return map->items[ndx].val;
}

void _sc_map_del(sc_vm* vm, _sc_map* map, sc_obj k) {
    int hash = sc_hash(vm, k);
    int ndx = _sc_map_hash_find(vm, map, hash, k);
    if (ndx < 0) {
        /* TODO ���-������ ���� ���������� ����*/
    }
    map->items[ndx].used = -1;
    map->size--;
}

int _sc_map_next(sc_vm* vm, _sc_map* map) {
    if (map->size == 0) {
        /* TODO ���-������ ���� ���������� ����*/
    }
    while (1) {
        map->cur = (map->cur + 1) & map->mask;
        if (map->items[map->cur].used > 0) {
            return map->cur;
        }
    }
}

sc_obj sc_map(sc_vm* vm) {
    sc_obj obj = { SC_MAP };
    obj.map.val = _sc_map_new();
    if (vm) {
        return sc_gc_track(vm, obj);
    } else {
        return obj;
    }
}

sc_obj sc_map_n(sc_vm* vm, int num, sc_obj* objs) {
    sc_obj obj = sc_map(vm);
    int i;
    for (i = 0; i < num; i++) {
        sc_set(vm, obj, objs[i * 2], objs[i * 2 + 1]);
    }
    return obj;
}

sc_obj sc_map_copy(sc_vm* vm, sc_obj rhs) {
    sc_obj new_map = { SC_MAP };
    new_map.map.val = _sc_map_new(vm);
    new_map.map.val = rhs.map.val;
    new_map.map.val->mark = 0;
    new_map.map.val->items = (_sc_map_item*)malloc(sizeof(_sc_map_item) * rhs.map.val->capacity);
    memcpy(new_map.map.val->items, rhs.map.val->items, sizeof(_sc_map_item) * rhs.map.val->capacity);
    return sc_gc_track(vm, new_map);
}