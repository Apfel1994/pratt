#include "sc.h"

#include <stdlib.h>

/* sc_gc.c */
sc_obj sc_gc_track(sc_vm* vm, sc_obj o);

sc_obj sc_func(sc_vm* vm, int type, void* jmp, sc_obj const_pars, sc_obj globals) {
    sc_obj ret = { SC_FUNC };
    _sc_func* val = (_sc_func*)calloc(1, sizeof(_sc_func));
    ret.func.jmp_point = jmp;
    val->const_pars = const_pars;
    val->globals = globals;
    val->type = type;
    ret.func.val = val;
    return sc_gc_track(vm, ret);
}

sc_obj sc_cfunc(sc_vm* vm, sc_obj(*p_f)(sc_vm*)) {
    return sc_func(vm, 1, (void*)p_f, sc_null, sc_null);
}
