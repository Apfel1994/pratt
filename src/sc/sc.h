#ifndef SC_H
#define SC_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct sc_number_ {
    int type;
    double val;
} sc_number_;

typedef struct sc_string_ {
    int type;
    struct _sc_string* str;
    const char* val;
    int size;
} sc_string_;

typedef struct sc_vector_ {
    int type;
    struct _sc_vector* val;
} sc_vector_;

typedef struct sc_map_ {
    int type;
    struct _sc_map* val;
} sc_map_;

union sc_obj;

typedef struct sc_func_ {
    int type;
    struct _sc_func* val;
    void* jmp_point;
} sc_func_;

typedef struct sc_cdata_ {
    int type;
    struct _sc_cdata* val;
    void* ptr;
} sc_cdata_;

typedef union sc_obj {
    int type;
    sc_number_ number;
    sc_string_ string;
    sc_vector_ vector;
    sc_map_ map;
    sc_func_ func;
    sc_cdata_ cdata;
    struct {
        int type;
        int* p_mark;
    } gc;
} sc_obj;

typedef struct sc_code {
    unsigned char i, a, b, c;
} sc_code;

typedef struct _sc_string {
    int mark;
    char s[1];
} _sc_string;

typedef struct _sc_vector {
    int mark;
    sc_obj* items;
    int size;
    int capacity;
} _sc_vector;

typedef struct _sc_map_item {
    int used;
    int hash;
    sc_obj key;
    sc_obj val;
} _sc_map_item;

typedef struct _sc_map {
    int mark;
    _sc_map_item* items;
    int mask;
    int cur;
    int size;
    int capacity;
    int num_non_empty;
} _sc_map;

typedef struct _sc_func {
    int mark;
    sc_obj const_pars;
    sc_obj globals;
    int type;
} _sc_func;

typedef struct sc_frame {
    sc_code* codes;
    sc_code* ip;
    sc_obj* regs;
    int num_regs;
    sc_obj globals;
    sc_obj func_name;
    sc_obj file_name;
    sc_obj* ret_dest;
} sc_frame;

typedef struct sc_vm {
    sc_obj* const_pool;
    sc_obj _regs_vec;
    sc_obj* regs;
    sc_obj root;
    sc_obj user_defined;
    sc_obj modules;
    sc_frame frames[256];
    int cur_frame;
    unsigned char chars[256][2];

    sc_obj cur_params_vec;
    sc_obj params_vec;

    /**/
    _sc_vector* white;
    _sc_vector* grey;
    _sc_vector* black;
    int gc_steps;
} sc_vm;

typedef struct _sc_cdata {
    int mark;
    void (*p_free)(sc_vm* vm, sc_obj obj);
} _sc_cdata;

enum types {
    SC_NULL, SC_NUM, SC_STR, SC_VEC, SC_MAP, SC_FUNC, SC_CDATA
};

enum {
    SC_IEOF = 0,
    SC_ICOPY,
    SC_IADD,
    SC_ISUB,
    SC_IMUL,
    SC_IDIV,
    SC_IIF,
    SC_IEQ,
    SC_INE,
    SC_ILT,
    SC_ILE,
    SC_IJMP,
    SC_IGC_COLLECT,
    SC_INUM,
    SC_ISTR,
    SC_IVEC,
    SC_IMAP,
    SC_IGET,
    SC_IIGET,
    SC_IGGET,
    SC_ISET,
    SC_IGSET,
    SC_IFUNC,
    SC_IREGS,
    SC_ICALL,
    SC_IRETURN,
    SC_IFUNCNAME,
    SC_IFILENAME,
    SC_IPARAMS,
    SC_INULL
};

extern char* i_names[];

extern sc_obj sc_null;

sc_vm* sc_new_vm();
void sc_deinit_vm(sc_vm* vm);
void sc_run(sc_vm* vm);
void sc_register_func(sc_vm* vm, const char* name, sc_obj (*p_f)(sc_vm*));
void sc_include_file(sc_vm* vm, const char* name, const char* file_name);
void sc_include_file_content(sc_vm* vm, const char* fname, const char* file_name, const char* file_cont, int len);
void sc_unload(sc_vm* vm, const char* name);

int sc_print_code(sc_code* codes, int i);

sc_obj sc_get_param(sc_vm* vm);

sc_obj sc_number(double val);

sc_obj sc_string(char* val, int len);
sc_obj sc_string_s(const char* val);
sc_obj sc_string_t(sc_vm* vm, int len);
sc_obj sc_string_f(sc_vm* vm, const char* fmt, ...);
sc_obj sc_string_cpy(sc_vm *vm, const char *str, int len);

sc_obj sc_vector(sc_vm* vm);
sc_obj sc_vector_n(sc_vm* vm, int n, sc_obj *objs);
sc_obj sc_vector_v(sc_vm* vm, int n, ...);
sc_obj sc_map(sc_vm* vm);

sc_obj sc_call(sc_vm* vm, const char* mod, const char* fnc, sc_obj params);

sc_obj sc_get(sc_vm* vm, sc_obj obj, sc_obj k);
void sc_set(sc_vm* vm, sc_obj obj, sc_obj k, sc_obj v);

sc_obj sc_to_string(sc_vm* vm, sc_obj obj);
sc_obj sc_print(sc_vm* vm);

#ifdef __cplusplus
}
#endif

#endif
