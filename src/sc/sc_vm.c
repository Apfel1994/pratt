/*
 ================================================================================

 The tinypy License

 Copyright (c) 2008 Phil Hassey

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

 ================================================================================
 */
#pragma warning(disable : 4996)

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include <math.h>

#include "sc.h"

sc_obj sc_null = { SC_NULL };

char* i_names[] = { "SC_IEOF", "SC_ICOPY", "SC_IADD", "SC_ISUB", "SC_IMUL", "SC_IDIV", "SC_IIF", "SC_IEQ", "SC_INE",
        "SC_ILT", "SC_ILE", "SC_IJMP", "SC_IGC_COLLECT", "SC_INUM", "SC_ISTR", "SC_IVEC", "SC_IMAP", "SC_IGET",
        "SC_IIGET", "SC_IGGET", "SC_ISET", "SC_IGSET", "SC_IFUNC", "SC_IREGS", "SC_ICALL", "SC_IRETURN", "SC_IFUNCNAME",
        "SC_IFILENAME", "SC_IPARAMS", "SC_INULL" };

_sc_vector* _sc_vector_new();
void _sc_vector_free(_sc_vector* vec);
void _sc_vector_resize(_sc_vector* vec, int len);
void _sc_vector_set(sc_vm* vm, _sc_vector* vec, int k, sc_obj v);
sc_obj _sc_vector_get(sc_vm* vm, _sc_vector* vec, int k);
void _sc_vector_insert(sc_vm* vm, _sc_vector* vec, int k, sc_obj v);
void _sc_vector_push_back(sc_vm* vm, _sc_vector* vec, sc_obj v);
void _sc_vector_insert_nogrey(sc_vm* vm, _sc_vector* vec, int k, sc_obj v);
void _sc_vector_push_back_nogrey(sc_vm* vm, _sc_vector* vec, sc_obj v);
sc_obj _sc_vector_pop(sc_vm* vm, _sc_vector* vec, int k);
void _sc_vector_shrink_to_fit(sc_vm* vm, _sc_vector* vec);
int _sc_vector_find(sc_vm* vm, _sc_vector* vec, sc_obj k);
sc_obj sc_push_back(sc_vm *vm);

sc_obj sc_map(sc_vm* vm);
sc_obj sc_map_n(sc_vm* vm, int size, sc_obj* objs);
sc_obj sc_map_copy(sc_vm* vm, sc_obj rhs);
_sc_map* _sc_map_new();
void _sc_map_free(_sc_map* map);
void _sc_map_resize(_sc_map* map, int new_size);
void _sc_map_hash_set(_sc_map* map, int hash, sc_obj k, sc_obj v);
int _sc_map_hash_find(sc_vm* vm, _sc_map* map, int hash, sc_obj k);
void _sc_map_flush_unused(_sc_map* map);
void _sc_map_set(sc_vm* vm, _sc_map* map, sc_obj k, sc_obj v);
void _sc_map_set_nogrey(sc_vm* vm, _sc_map* map, sc_obj k, sc_obj v);
sc_obj _sc_map_get(sc_vm* vm, _sc_map* map, sc_obj k);
void _sc_map_del(sc_vm* vm, _sc_map* map, sc_obj k);
int _sc_map_next(sc_vm* vm, _sc_map* map);

sc_obj sc_func(sc_vm* vm, int type, void* jmp, sc_obj const_pars, sc_obj globals);
void sc_push_frame(sc_vm* vm, sc_obj* dest, sc_obj globals, sc_code* codes);
void _sc_call(sc_vm* vm, sc_obj* dest, sc_obj func, sc_obj params);

sc_obj sc_cdata(sc_vm* vm, void* ptr, void* free_func);

sc_obj sc_add(sc_vm* vm, sc_obj a, sc_obj b);
sc_obj sc_sub(sc_vm* vm, sc_obj a, sc_obj b);
sc_obj sc_mul(sc_vm* vm, sc_obj a, sc_obj b);
sc_obj sc_div(sc_obj a, sc_obj b);
int sc_cmp(sc_vm* vm, sc_obj a, sc_obj b);
int sc_bool(sc_vm* vm, sc_obj a);
sc_obj sc_get(sc_vm* vm, sc_obj obj, sc_obj k);
int sc_iget(sc_vm* vm, sc_obj* ret, sc_obj obj, sc_obj k);
void sc_set(sc_vm* vm, sc_obj obj, sc_obj k, sc_obj v);

int sc_hash(sc_vm* vm, sc_obj obj);

void sc_add_to_grey(sc_vm* vm, sc_obj v);
void sc_follow(sc_vm* vm, sc_obj v);
void sc_gc_reset(sc_vm* vm);
void sc_gc_collect(sc_vm* vm);
void sc_delete(sc_vm* vm, sc_obj o);
void sc_gc_process_grey(sc_vm* vm);
void sc_gc_step(sc_vm* vm);
void sc_full(sc_vm* vm);
sc_obj sc_gc_track(sc_vm* vm, sc_obj o);

sc_obj sc_to_string(sc_vm* vm, sc_obj obj);
sc_obj sc_print(sc_vm* vm);

sc_obj sc_func(sc_vm* vm, int type, void* jmp, sc_obj const_pars, sc_obj globals);
sc_obj sc_cfunc(sc_vm* vm, sc_obj(*p_f)(sc_vm*));

sc_obj sc_cdata(sc_vm* vm, void* ptr, void* free_func);

/**********************************************************************/

sc_obj sc_load(sc_vm* vm) {
    sc_obj name = vm->cur_params_vec.vector.val->items[0];
    char name_str[128];
    strcpy(name_str, name.string.val);
    sc_obj m;
    int n = _sc_map_hash_find(vm, vm->modules.map.val, sc_hash(vm, name), name);
    if (n == -1) {
        assert(vm->cur_params_vec.vector.val->size == 2);
        sc_obj name1 = vm->cur_params_vec.vector.val->items[1];
        sc_frame* f = &vm->frames[vm->cur_frame];
        char name1_str[128];
        strcpy(name1_str, f->file_name.string.val);
        char* p1 = name1_str + strlen(name1_str);
        while (p1 > name1_str && *p1 != '/')
            p1--;
        *++p1 = '\0';
        strcat(name1_str, name1.string.val);
        printf("including %s %s\n", name_str, name1_str);
        sc_include_file(vm, name_str, name1_str);
        m = sc_get(vm, vm->modules, name);
        return m;
    } else {
        return vm->modules.map.val->items[n].val;
    }
}

/**********************************************************************/

void sc_builtins(sc_vm* vm) {
    sc_register_func(vm, "load_module", &sc_load);
    sc_register_func(vm, "print", &sc_print);
}

void sc_init_vm(sc_vm* vm) {
    int i;
    vm->root = sc_vector(0);
    /**/
    for (i = 0; i < 256; i++) {
        vm->chars[i][0] = i;
        vm->chars[i][1] = '\0';
    }
    /**/
    vm->cur_frame = -1;

    /**/
    vm->white = _sc_vector_new();
    vm->grey = _sc_vector_new();
    vm->black = _sc_vector_new();
    vm->gc_steps = 0;
    /**/

    vm->_regs_vec = sc_vector(vm);
    for (i = 0; i < 8192 * 2; i++) {
        _sc_vector_push_back(vm, vm->_regs_vec.vector.val, sc_null);
    }
    vm->regs = vm->_regs_vec.vector.val->items;

    vm->user_defined = sc_map(vm);
    vm->modules = sc_map(vm);

    vm->params_vec = sc_vector(vm);
    for (i = 0; i < 256; i++) {
        _sc_vector_push_back(vm, vm->params_vec.vector.val, sc_vector(vm));
    }
    vm->cur_params_vec = sc_null;

    _sc_vector_push_back(vm, vm->root.vector.val, vm->_regs_vec);
    _sc_vector_push_back(vm, vm->root.vector.val, vm->user_defined);
    _sc_vector_push_back(vm, vm->root.vector.val, vm->params_vec);
    _sc_vector_push_back(vm, vm->root.vector.val, vm->modules);
    /**/
    sc_builtins(vm);

    sc_full(vm);
}

void sc_deinit_vm(sc_vm* vm) {
    while (vm->root.vector.val->size) {
        _sc_vector_pop(vm, vm->root.vector.val, vm->root.vector.val->size - 1);
    }
    sc_full(vm);
    sc_full(vm);
    sc_delete(vm, vm->root);
    /**/
    _sc_vector_free(vm->white);
    _sc_vector_free(vm->grey);
    _sc_vector_free(vm->black);
    /**/
    free(vm);
}

sc_vm* sc_new_vm() {
    sc_vm* vm = (sc_vm*) calloc(1, sizeof(sc_vm));
    sc_init_vm(vm);
    return vm;
}

void sc_push_frame(sc_vm* vm, sc_obj* dest, sc_obj globals, sc_code* codes) {
    sc_frame new_frame;
    new_frame.codes = codes;
    new_frame.ip = new_frame.codes;
    new_frame.ret_dest = dest;
    new_frame.globals = globals;
    if (vm->cur_frame == -1) {
        new_frame.regs = vm->regs;
    } else {
        new_frame.regs = vm->frames[vm->cur_frame].regs + vm->frames[vm->cur_frame].num_regs;
    }
    vm->cur_frame++;
    vm->frames[vm->cur_frame] = new_frame;
}

void _sc_call(sc_vm* vm, sc_obj* dest, sc_obj func, sc_obj params) {
    if (func.type == SC_FUNC) {
        if (func.func.val->type == 0) {
            sc_push_frame(vm, dest, func.func.val->globals, (sc_code*) func.func.jmp_point);
            vm->frames[vm->cur_frame].regs[0] = params;
		} else if (func.func.val->type == 1 || func.func.val->type == 2) {
			if (func.func.val->type == 2) {
				sc_push_frame(vm, dest, func.func.val->globals, (sc_code*)func.func.jmp_point);
			}

			vm->frames[vm->cur_frame].regs[0] = params;
			if (func.func.val->const_pars.type != SC_NULL) {
                int i;
				for (i = 0; i < func.func.val->const_pars.vector.val->size; i++) {
					_sc_vector_insert(vm, params.vector.val, i, func.func.val->const_pars.vector.val->items[i]);
				}
			}

			if (func.func.val->type == 1) {
				(*dest) = ((sc_obj(*)(sc_vm*)) func.func.jmp_point)(vm);
				sc_add_to_grey(vm, *dest);
			}
        }
        return;
    }
    printf("func is not callable (%d)", func.type);
}

sc_obj sc_call(sc_vm* vm, const char *mod, const char *fnc, sc_obj params) {
    sc_obj tmp;
    sc_obj r = sc_null;
    tmp = sc_get(vm, vm->modules, sc_string_s(mod));
    tmp = sc_get(vm, tmp, sc_string_s(fnc));
    _sc_call(vm, &r, tmp, params);
    sc_run(vm);
    return r;
}

sc_obj sc_params(sc_vm* vm) {
    sc_obj ret;
    vm->cur_params_vec = vm->params_vec.vector.val->items[vm->cur_frame];
    ret = vm->cur_params_vec;
    ret.vector.val->size = 0;
    return ret;
}

sc_obj sc_params_n(sc_vm* vm, int n, sc_obj* pars) {
    sc_obj ret = sc_params(vm);
    int i;
    _sc_vector_resize(ret.vector.val, n);
    for (i = 0; i < n; i++) {
        _sc_vector_push_back(vm, ret.vector.val, pars[i]);
    }
    return ret;
}

#define RA f->regs[ip->a]
#define RB f->regs[ip->b]
#define RC f->regs[ip->c]

#define VA ((int)ip->a)
#define VB ((int)ip->b)
#define VC ((int)ip->c)

#define SHORT_AB (*((short*)(&ip->a)))
#define SHORT_BC (*((short*)(&ip->b)))

#define GA sc_add_to_grey(vm, RA)

//#define PRINT_CODES

void sc_run(sc_vm* vm) {
    sc_frame* f = &vm->frames[vm->cur_frame];
    sc_code* ip = f->ip;
    while (1) {
#ifdef PRINT_CODES
        sc_print_code(ip, 0);
#endif
        fflush(stdout);
        switch (ip->i) {
            case SC_IEOF:
                return;
            case SC_IADD: RA = sc_add(vm, RB, RC); break;
            case SC_ISUB: RA = sc_sub(vm, RB, RC); break;
            case SC_IMUL: RA = sc_mul(vm, RB, RC); break;
            case SC_IDIV: RA = sc_div(RB, RC); break;
            case SC_ICOPY: RA = RB; break;
            case SC_IEQ: RA = sc_number(sc_cmp(vm, RB, RC) == 0); break;
            case SC_INE: RA = sc_number(sc_cmp(vm, RB, RC)!=0); break;
            case SC_ILT: RA = sc_number(sc_cmp(vm, RB, RC)<0); break;
            case SC_ILE: RA = sc_number(sc_cmp(vm, RB, RC)<=0); break;
            case SC_IIF: if(sc_bool(vm, RA)) ip++; break;
            case SC_IJMP: ip += SHORT_AB; continue; break;
            /*case SC_IPRINT:		sc_print(vm, RA); break;*/
            case SC_IGC_COLLECT:sc_full(vm); sc_full(vm); break;
            case SC_INUM: RA = sc_number(*(double*)(ip+1)); ip+=2; break;
            case SC_ISTR: RA = sc_string((char*)(ip+1), SHORT_BC); ip += SHORT_BC/4 + 1; break;
            case SC_IVEC: RA = sc_vector_n(vm, VB, &RC); break;
            case SC_IMAP: RA = sc_map_n(vm, VB, &RC); break;
            case SC_IGET: RA = sc_get(vm, RB, RC); GA; break;
            case SC_IIGET: sc_iget(vm, &RA, RB, RC); break;
            case SC_IGGET:
                if (!sc_iget(vm, &RA, f->globals, RB)) {
                    if (!sc_iget(vm, &RA, vm->user_defined, RB)) {
                        printf("var not found\n");
                        system("pause");
                    }
                }
            break;
            case SC_ISET: sc_set(vm, RA, RB, RC); break;
            case SC_IGSET: sc_set(vm, f->globals, RB, RC); break;
            case SC_IFUNC: RA = sc_func(vm, 0, (ip+1), sc_null, f->globals); ip += SHORT_BC; break;
            case SC_IREGS: f->num_regs = VA; break;
            case SC_ICALL: f->ip = ip+1; _sc_call(vm, &RA, RB, RC); f = &vm->frames[vm->cur_frame]; ip = f->ip; continue;
            case SC_IRETURN:
            if(f->ret_dest) {
                *(f->ret_dest) = RA;
                sc_add_to_grey(vm, RA);
            }
            //printf("%i\n", vm->cur_frame);
            memset(f->regs, 0, f->num_regs * sizeof(sc_obj));
            vm->cur_frame--;
            if(vm->cur_frame == -1)return;
            f = &vm->frames[vm->cur_frame];
            ip = f->ip;
            continue;
            case SC_IFILENAME: f->file_name = RA; break;
            case SC_IFUNCNAME: f->func_name = RA; break;
            case SC_IPARAMS: RA = sc_params_n(vm, VB, &RC); break;
            case SC_INULL: RA = sc_null; break;
            default:
            printf("Unknown instruction! %i", (int)(ip->i));
            exit(0);
        }
        ip++;
    }
}

void sc_register_func(sc_vm* vm, const char* name, sc_obj (*p_f)(sc_vm*)) {
    sc_set(vm, vm->user_defined, sc_string((char*) name, strlen(name)), sc_cfunc(vm, p_f));
}

void* sc_compile_file(const char* fname) { return NULL; }
void* sc_compile_file_content(const char* fname, const char* src, int len) { return NULL; }

void sc_free_codes(sc_vm* vm, sc_obj obj) {
    free(obj.cdata.ptr);
    printf("CODES DELETED!!!!\n");
}

void sc_include_codes_malloced(sc_vm* vm, const char* name, sc_code* codes) {
    sc_obj new_module = sc_map(vm);
    sc_obj o_name = sc_string_cpy(vm, name, strlen(name));
    sc_set(vm, new_module, sc_string_s("MODULE_NAME"), o_name);
    sc_set(vm, new_module, sc_string_s("MODULE_CODES"), sc_cdata(vm, (void*) codes, &sc_free_codes));
    sc_push_frame(vm, 0, new_module, (sc_code*) codes);
    sc_set(vm, vm->modules, o_name, new_module);
    sc_run(vm);
    vm->cur_frame--;
}

void sc_include_codes_const(sc_vm* vm, const char* name, const sc_code* codes) {
    const sc_code *c = codes;
    while (c->i != SC_IEOF) {
        c++;
    }
    size_t size = (c - codes + 1) * sizeof(sc_code);
    sc_code *_codes = malloc(size);
    memcpy(_codes, codes, size);
    sc_include_codes_malloced(vm, name, _codes);
}

void sc_unload(sc_vm* vm, const char* name) {
    _sc_map_del(vm, vm->modules.map.val, sc_string_s(name));
}

void sc_include_file(sc_vm* vm, const char* name, const char* file_name) {
    sc_code* codes = sc_compile_file(file_name);
    if (!codes) {
        return;
    }
    sc_include_codes_malloced(vm, name, codes);
}

void sc_include_file_content(sc_vm* vm, const char* name, const char* file_name, const char* file_cont, int len) {
    sc_code* codes = sc_compile_file_content(file_name, file_cont, len);
    if (!codes) {
        return;
    }
    sc_include_codes_malloced(vm, name, codes);
}

int sc_print_code(sc_code* codes, int i) {
    short ll;
    sc_code code = codes[i];
    switch (code.i) {
    case SC_INUM:
        printf("NUM \t%d \t%.2f\n", code.a, *(double*) (&codes[i + 1]));
        i += 2;
        break;
    case SC_ICOPY:
        printf("COPY \t%d  <- \t%d\n", code.a, code.b);
        break;
    case SC_ISTR:
        printf("STR \t%d \t\"", code.a);
        ll = *(short*) &code.b;
        //sc_print_n_chars((char*)&codes[i + 1], ll);
        printf("%.*s", ll, (char*) &codes[i + 1]);
        //i += (ll+3)/4;
        i += ll / 4 + 1;
        printf("\"\n");
        break;
    default:
        //assert(code.i < 29);
        printf("%s \t%d \t%d \t%d\n", i_names[code.i] + 4, code.a, code.b, code.c);
        //printf(" \t%d \t%d \t%d\n", code.args.a, code.args.b, code.args.c);
        break;
    }
    return i;
}

sc_obj sc_get_param(sc_vm* vm) {
    return _sc_vector_pop(vm, vm->cur_params_vec.vector.val, 0);
}
