#include "sc.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#pragma warning(disable : 4996)

/* sc_gc.c */
sc_obj sc_gc_track(sc_vm* vm, sc_obj o);

sc_obj sc_string(char* val, int len) {
    sc_obj o = { SC_STR };
    o.string.val = val;
    o.string.size = len;
    o.string.str = 0;

    return o;
}

sc_obj sc_string_t(sc_vm* vm, int len) {
    sc_obj o = sc_string(0, len);
    o.string.str = (_sc_string*)calloc(1, sizeof(_sc_string) + len);
    o.string.val = o.string.str->s;
    return o;
}

sc_obj sc_string_s(const char* val) {
    sc_obj o = { SC_STR };
    o.string.size = (int)strlen(val);
    o.string.val = val;
    return o;
}

sc_obj sc_string_f(sc_vm* vm, const char* fmt, ...) {
    int l;
    char* s;
    va_list arg;
    sc_obj ret;
    va_start(arg, fmt);
    l = vsnprintf(NULL, 0, fmt, arg);
    ret = sc_string_t(vm, l);
    s = ret.string.str->s;
    va_end(arg);
    va_start(arg, fmt);
    vsprintf(s, fmt, arg);
    va_end(arg);
    return sc_gc_track(vm, ret);
}

sc_obj sc_string_cpy(sc_vm *vm, const char *str, int len) {
    size_t size = len ? len : strlen(str);
    sc_obj ret = sc_string_t(vm, size);
    memcpy(ret.string.str->s, str, (size_t)size);
    return sc_gc_track(vm, ret);
}