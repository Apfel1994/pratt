#ifndef SC_STR_POOL
#define SC_STR_POOL

typedef struct sc_str_pool {
    struct sc_str_pool_blck *tail;
} sc_str_pool;

void sc_str_pool_init(sc_str_pool *p);
void sc_str_pool_destroy(sc_str_pool *p);

char *sc_str_pool_alloc(sc_str_pool *p, const char *s, int len);

#endif /* SC_STR_POOL */