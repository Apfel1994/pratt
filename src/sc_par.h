#ifndef SC_PAR_H
#define SC_PAR_H

#include "sc_syms.h"
#include "sc_tok.h"

struct sc_scope;

typedef struct sc_par_state {
	sc_toks toks;
	sc_syms syms;
    struct sc_scope *scope;
	sc_sym *token;
	int token_nr;
} sc_par_state;

void sc_par_state_init(sc_par_state *state);
void sc_par_state_destroy(sc_par_state *state);
sc_sym *sc_par_create_symbol(sc_par_state *state, const char *id, int bp);
sc_sym *sc_par_create_constant(sc_par_state *state, const char *id, const char *val, sc_nud_func nud);

int sc_par_tokenize(sc_par_state *state, const char *str);

struct sc_scope *sc_par_push_scope(sc_par_state *state);
void sc_par_pop_scope(sc_par_state *state);
sc_sym *sc_par_find_in_scope(sc_par_state *state, const char *v);

sc_sym *sc_par_advance(sc_par_state *state, const char *id);
sc_sym *sc_par_expression(sc_par_state *state, int rbp);

void sc_par_statement(sc_par_state *state, sc_syms *ret);
sc_syms sc_par_statements(sc_par_state *state);

#endif /* SC_PAR_H */
